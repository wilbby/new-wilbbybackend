"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendEmailToList = void 0;
const nodemailer = require("nodemailer");
const emailsDaily_1 = __importDefault(require("./emailsDaily"));
exports.SendEmailToList = (maillist) => {
    maillist.forEach((to) => {
        const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            service: "gmail",
            port: 465,
            secure: true,
            auth: {
                user: process.env.EMAIL_ADDRESS,
                pass: process.env.EMAIL_PASSWORD,
            },
        });
        const mailOptions = {
            from: process.env.EMAIL_ADDRESS,
            to: to,
            subject: "Hola Sushi Lovers de Wilbby ❤️",
            text: "Solo hoy en Wilbby tienes 20% de descuento en Ama Sushi Bar Burgos 🍣 🍱",
            html: emailsDaily_1.default(),
        };
        mailOptions.to = to;
        transporter.sendMail(mailOptions);
    });
};
