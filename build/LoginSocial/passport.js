"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const user_1 = __importDefault(require("../models/user"));
const mailJet_1 = require("../mailJet");
const SendEmail_1 = require("../RecoveryPassword/Email/SendEmail");
const saveEmail_1 = require("../SaveEmailList/saveEmail");
var passport = require("passport");
var FacebookTokenStrategy = require("passport-facebook-token");
var GoogleTokenStrategy = require("passport-google-token").Strategy;
var TwitterTokenStrategy = require("passport-twitter-token");
module.exports = function () {
    passport.use(new TwitterTokenStrategy({
        consumerKey: config_1.twiterr_api_key,
        consumerSecret: config_1.twitter_api_secret,
        includeEmail: true,
    }, (token, tokenSecret, profile, done) => __awaiter(this, void 0, void 0, function* () {
        require("mongoose")
            .model("user")
            .schema.add({ isSocial: Boolean, socialNetworks: String });
        // // check if email exists
        let emailExists = yield user_1.default.findOne({
            email: profile.emails[0].value,
        });
        mailJet_1.SaveEmail(profile.emails[0].value, profile.name.givenName);
        const my = Object.assign({}, emailExists); // && my._doc.isSocial
        if (emailExists) {
            bcryptjs_1.default.genSalt(10, (err, salt) => {
                if (err)
                    console.log(err);
                bcryptjs_1.default.hash(token, salt, (err, hash) => {
                    if (err)
                        console.log(err);
                    user_1.default.findOneAndUpdate({ email: profile.emails[0].value }, { password: hash }, (err, updated) => {
                        if (err)
                            console.log(err);
                        let nuevoUsuario = updated;
                        return done(err, { nuevoUsuario, token: token });
                    });
                });
            });
        }
        SendEmail_1.welcomeEmail(profile.emails[0].value, profile.name.givenName);
        saveEmail_1.SaveEmailWilbby(profile.emails[0].value, profile.name.givenName, profile.name.familyName);
        const nuevoUsuario = new user_1.default({
            id: profile.id,
            password: token,
            email: profile.emails[0].value,
            name: profile.name.givenName,
            lastName: profile.name.familyName,
            isSocial: true,
            socialNetworks: "Twitter",
        });
        nuevoUsuario.id = nuevoUsuario._id;
        nuevoUsuario.save((error) => {
            return done(error, { nuevoUsuario, token: token });
        });
    })));
    passport.use(new FacebookTokenStrategy({
        clientID: config_1.FACEBOOK_APP_ID,
        clientSecret: config_1.FACEBOOK_SECRET,
    }, (accessToken, refreshToken, profile, done) => __awaiter(this, void 0, void 0, function* () {
        require("mongoose")
            .model("user")
            .schema.add({ isSocial: Boolean, socialNetworks: String });
        mailJet_1.SaveEmail(profile.emails[0].value, profile.name.givenName);
        let emailExists = yield user_1.default.findOne({
            email: profile.emails[0].value,
        });
        if (emailExists) {
            bcryptjs_1.default.genSalt(10, (err, salt) => {
                if (err)
                    console.log(err);
                bcryptjs_1.default.hash(accessToken, salt, (err, hash) => {
                    if (err)
                        console.log(err);
                    user_1.default.findOneAndUpdate({ email: profile.emails[0].value }, { password: hash }, (err, updated) => {
                        if (err)
                            console.log(err);
                        let nuevoUsuario = updated;
                        return done(err, { nuevoUsuario, token: accessToken });
                    });
                });
            });
        }
        else {
            SendEmail_1.welcomeEmail(profile.emails[0].value, profile.name.givenName);
            saveEmail_1.SaveEmailWilbby(profile.emails[0].value, profile.name.givenName, profile.name.familyName);
            const nuevoUsuario = new user_1.default({
                id: profile.id,
                password: accessToken,
                email: profile.emails[0].value,
                name: profile.name.givenName,
                lastName: profile.name.familyName,
                isSocial: true,
                socialNetworks: "Facebook",
            });
            nuevoUsuario.id = nuevoUsuario._id;
            nuevoUsuario.save((error) => {
                return done(error, { nuevoUsuario, token: accessToken });
            });
        }
    })));
    passport.use(new GoogleTokenStrategy({
        clientID: config_1.GOOGLE_CLIENT_ID,
        clientSecret: config_1.google_customer_secret,
    }, (accessToken, refreshToken, profile, done) => __awaiter(this, void 0, void 0, function* () {
        require("mongoose")
            .model("user")
            .schema.add({ isSocial: Boolean, socialNetworks: String });
        mailJet_1.SaveEmail(profile.emails[0].value, profile.name.givenName);
        // check if email exists
        let emailExists = yield user_1.default.findOne({
            email: profile.emails[0].value,
        });
        if (emailExists) {
            bcryptjs_1.default.genSalt(10, (err, salt) => {
                if (err)
                    console.log(err);
                bcryptjs_1.default.hash(accessToken, salt, (err, hash) => {
                    if (err)
                        console.log(err);
                    user_1.default.findOneAndUpdate({ email: profile.emails[0].value }, { password: hash }, (err, updated) => {
                        if (err)
                            console.log(err);
                        console.log(updated);
                        let nuevoUsuario = updated;
                        return done(err, { nuevoUsuario, token: accessToken });
                    });
                });
            });
        }
        else {
            SendEmail_1.welcomeEmail(profile.emails[0].value, profile.name.givenName);
            saveEmail_1.SaveEmailWilbby(profile.emails[0].value, profile.name.givenName, profile.name.familyName);
            const nuevoUsuario = new user_1.default({
                id: profile.id,
                password: accessToken,
                email: profile.emails[0].value,
                name: profile.name.givenName,
                lastName: profile.name.familyName,
                isSocial: true,
                socialNetworks: "Google",
            });
            nuevoUsuario.id = nuevoUsuario._id;
            nuevoUsuario.save((error) => {
                return done(error, { nuevoUsuario, token: accessToken });
            });
        }
    })));
};
