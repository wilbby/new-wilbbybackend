import { RatingCalculator } from "../funtions/RatingCalculartor";
import ratingSchema from "../models/rating";
import restaurantSchema from "../models/restaurant";

export const updateRestaurant = async (id) => {
  const g = await restaurantSchema.findOne({ _id: id });

  const ratings = await ratingSchema.find({ restaurant: g?._id });
  const averageRating = RatingCalculator(ratings ? ratings : []);
  restaurantSchema.findOneAndUpdate(
    { _id: g?._id },
    { rating: averageRating ? `${averageRating.toFixed(1)}` : "0.0" },
    () => {
      console.log("done");
    }
  );
};
