export const datas = (data: any, total: number, id: number) => {
  const items = [
    {
      name: "Servicio de envío",
      desc: "Envío Wilbby",
      units: 1,
      subtotal: total / 100,
    },
  ];

  return {
    applyContactDefaults: false,
    contactCode: data.contactCode,
    desc: "Pago de envío",
    date: new Date(),
    dueDate: new Date(),
    notes: "Iva descontado del total",
    items: items,
    invoiceNum: id,
    currency: "EUR",
  };
};
