"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateOrder = void 0;
const config_1 = require("./config");
const data_1 = require("./data");
var request = require("request");
var details = {
    client_id: config_1.client_id,
    client_secret: config_1.client_secret,
    grant_type: "client_credentials",
    scope: "orders.status_update",
};
let formBody = [];
for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    //@ts-ignore
    formBody.push(encodedKey + "=" + encodedValue);
}
//@ts-ignore
formBody = formBody.join("&");
var options = {
    method: "POST",
    url: config_1.authURL,
    headers: {
        "Content-Type": "application/x-www-form-urlencoded",
    },
    body: formBody,
};
exports.UpdateOrder = (order, status) => {
    request(options, function (error, response) {
        const auth = JSON.parse(response.body);
        var opciones = {
            method: "POST",
            url: `${config_1.orderURL}/${order._id}/status`,
            headers: {
                "Content-Type": "application/json",
                "X-Application-Id": config_1.client_id,
                "X-Event-Id": order.eventId,
                "X-Store-Id": order.storeData.OtterPartnerId,
                Authorization: `Bearer ${auth.access_token}`,
            },
            body: JSON.stringify(data_1.dataUpdate(status)),
        };
        request(opciones, function (error, response) {
            console.log(response.body, response.statusCode, response.statusMessage);
        });
    });
};
