"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const cartSchema = new mongoose_1.default.Schema({
    userId: {
        type: String,
    },
    storeId: {
        type: String,
    },
    productId: {
        type: String,
    },
    items: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    addToCart: {
        type: Boolean,
        default: true,
    },
});
exports.default = mongoose_1.default.model("Newcard", cartSchema);
