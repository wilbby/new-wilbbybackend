"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const DounloadAppSchema = new mongoose_1.default.Schema({
    source: {
        type: String,
    },
    referred: {
        type: String,
    },
    date: {
        type: Date,
    },
    campaing: {
        type: String,
    },
});
exports.default = mongoose_1.default.model("Downloadapp", DounloadAppSchema);
