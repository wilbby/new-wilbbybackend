"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaveEmail = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: "variables.env" });
const mailjet = require("node-mailjet").connect(process.env.MJ_APIKEY_PUBLIC, process.env.MJ_APIKEY_PRIVATE);
function SaveEmail(email, name) {
    const request = mailjet.post("contact").request({
        Email: email,
        IsExcludedFromCampaigns: "false",
        Name: name,
    });
    request
        .then((result) => {
        const request = mailjet
            .post("contact")
            .id(result.body.Data[0].ID)
            .action("managecontactslists")
            .request({
            ContactsLists: [
                {
                    ListID: 35195,
                    Action: "addnoforce",
                },
            ],
        });
        request
            .then((result) => {
            console.log("done");
        })
            .catch((err) => {
            console.log(err.statusCode);
        });
    })
        .catch((err) => {
        console.log(err.statusCode);
    });
}
exports.SaveEmail = SaveEmail;
