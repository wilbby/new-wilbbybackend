"use strict";
import {
  FACEBOOK_APP_ID,
  GOOGLE_CLIENT_ID,
  google_customer_secret,
  FACEBOOK_SECRET,
  twiterr_api_key,
  twitter_api_secret,
} from "./config";
import bcrypt from "bcryptjs";
import userSchema from "../models/user";
import { SaveEmail } from "../mailJet";
import { welcomeEmail } from "../RecoveryPassword/Email/SendEmail";
import { SaveEmailWilbby } from "../SaveEmailList/saveEmail";
var passport = require("passport");
var FacebookTokenStrategy = require("passport-facebook-token");
var GoogleTokenStrategy = require("passport-google-token").Strategy;
var TwitterTokenStrategy = require("passport-twitter-token");

module.exports = function () {
  passport.use(
    new TwitterTokenStrategy(
      {
        consumerKey: twiterr_api_key,
        consumerSecret: twitter_api_secret,
        includeEmail: true,
      },
      async (token, tokenSecret, profile, done) => {
        require("mongoose")
          .model("user")
          .schema.add({ isSocial: Boolean, socialNetworks: String });
        // // check if email exists
        let emailExists = await userSchema.findOne({
          email: profile.emails[0].value,
        });

        SaveEmail(profile.emails[0].value, profile.name.givenName);

        const my = { ...emailExists }; // && my._doc.isSocial
        if (emailExists) {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) console.log(err);

            bcrypt.hash(token, salt, (err, hash) => {
              if (err) console.log(err);
              userSchema.findOneAndUpdate(
                { email: profile.emails[0].value },
                { password: hash },
                (err, updated) => {
                  if (err) console.log(err);

                  let nuevoUsuario = updated;
                  return done(err, { nuevoUsuario, token: token });
                }
              );
            });
          });
        }
        welcomeEmail(profile.emails[0].value, profile.name.givenName);
        SaveEmailWilbby(
          profile.emails[0].value,
          profile.name.givenName,
          profile.name.familyName
        );
        const nuevoUsuario = new userSchema({
          id: profile.id,
          password: token,
          email: profile.emails[0].value,
          name: profile.name.givenName,
          lastName: profile.name.familyName,
          isSocial: true,
          socialNetworks: "Twitter",
        });

        nuevoUsuario.id = nuevoUsuario._id;

        nuevoUsuario.save((error) => {
          return done(error, { nuevoUsuario, token: token });
        });
      }
    )
  );

  passport.use(
    new FacebookTokenStrategy(
      {
        clientID: FACEBOOK_APP_ID,
        clientSecret: FACEBOOK_SECRET,
      },
      async (accessToken: any, refreshToken: any, profile: any, done: any) => {
        require("mongoose")
          .model("user")
          .schema.add({ isSocial: Boolean, socialNetworks: String });
        SaveEmail(profile.emails[0].value, profile.name.givenName);
        let emailExists = await userSchema.findOne({
          email: profile.emails[0].value,
        });

        if (emailExists) {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) console.log(err);

            bcrypt.hash(accessToken, salt, (err, hash) => {
              if (err) console.log(err);
              userSchema.findOneAndUpdate(
                { email: profile.emails[0].value },
                { password: hash },
                (err, updated) => {
                  if (err) console.log(err);

                  let nuevoUsuario = updated;
                  return done(err, { nuevoUsuario, token: accessToken });
                }
              );
            });
          });
        } else {
          welcomeEmail(profile.emails[0].value, profile.name.givenName);
          SaveEmailWilbby(
            profile.emails[0].value,
            profile.name.givenName,
            profile.name.familyName
          );
          const nuevoUsuario = new userSchema({
            id: profile.id,
            password: accessToken,
            email: profile.emails[0].value,
            name: profile.name.givenName,
            lastName: profile.name.familyName,
            isSocial: true,
            socialNetworks: "Facebook",
          });
          nuevoUsuario.id = nuevoUsuario._id;

          nuevoUsuario.save((error) => {
            return done(error, { nuevoUsuario, token: accessToken });
          });
        }
      }
    )
  );

  passport.use(
    new GoogleTokenStrategy(
      {
        clientID: GOOGLE_CLIENT_ID,
        clientSecret: google_customer_secret,
      },
      async (accessToken: any, refreshToken: any, profile: any, done: any) => {
        require("mongoose")
          .model("user")
          .schema.add({ isSocial: Boolean, socialNetworks: String });
        SaveEmail(profile.emails[0].value, profile.name.givenName);
        // check if email exists
        let emailExists = await userSchema.findOne({
          email: profile.emails[0].value,
        });

        if (emailExists) {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) console.log(err);

            bcrypt.hash(accessToken, salt, (err, hash) => {
              if (err) console.log(err);

              userSchema.findOneAndUpdate(
                { email: profile.emails[0].value },
                { password: hash },
                (err, updated) => {
                  if (err) console.log(err);

                  console.log(updated);
                  let nuevoUsuario = updated;
                  return done(err, { nuevoUsuario, token: accessToken });
                }
              );
            });
          });
        } else {
          welcomeEmail(profile.emails[0].value, profile.name.givenName);
          SaveEmailWilbby(
            profile.emails[0].value,
            profile.name.givenName,
            profile.name.familyName
          );
          const nuevoUsuario = new userSchema({
            id: profile.id,
            password: accessToken,
            email: profile.emails[0].value,
            name: profile.name.givenName,
            lastName: profile.name.familyName,
            isSocial: true,
            socialNetworks: "Google",
          });
          nuevoUsuario.id = nuevoUsuario._id;

          nuevoUsuario.save((error) => {
            return done(error, { nuevoUsuario, token: accessToken });
          });
        }
      }
    )
  );
};
