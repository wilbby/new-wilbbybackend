"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.welcomeEmail = void 0;
const nodemailer = require("nodemailer");
const welcomeEmail_1 = __importDefault(require("./welcomeEmail"));
exports.welcomeEmail = (email, name) => {
    const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        service: "gmail",
        port: 465,
        secure: true,
        auth: {
            user: process.env.EMAIL_ADDRESS,
            pass: process.env.EMAIL_PASSWORD,
        },
    });
    const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: email ? email : "info@wilbby.com",
        subject: "Bienvenido a Wilbby",
        text: "Con Wilbby lo tienes todo",
        html: welcomeEmail_1.default(name),
    };
    transporter.sendMail(mailOptions);
};
