"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.datasHoldedCompra = void 0;
exports.datasHoldedCompra = (data) => {
    const items = data.items.map((element) => {
        const price = element.items.price / 100;
        const item = {
            name: element.items.name,
            desc: element.items.description,
            units: element.items.quantity,
            subtotal: price,
        };
        return item;
    });
    return {
        applyContactDefaults: false,
        contactCode: data.storeData && data.storeData.contactCode
            ? data.storeData.contactCode
            : "B09603440",
        desc: "Compra partner Wilbby",
        date: data.deliveryTime,
        dueDate: data.deliveryTime,
        notes: "Procentaje de tarifa no incluidos",
        items: items,
        invoiceNum: Number(data.channelOrderDisplayId),
        currency: "EUR",
    };
};
