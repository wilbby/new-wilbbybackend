"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.chargeToken = void 0;
const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
exports.chargeToken = (amount, stripeToken, currency) => __awaiter(void 0, void 0, void 0, function* () {
    const total = Number(amount).toFixed(0);
    let response = yield stripe.charges.create({
        amount: total,
        currency: currency ? currency : "EUR",
        source: stripeToken,
        capture: true,
    });
    return response;
});
