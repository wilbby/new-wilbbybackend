"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setOrderToDeliverect = void 0;
const request_1 = __importDefault(require("request"));
const config_1 = require("./config");
const data_1 = require("./data");
const node_cache_1 = __importDefault(require("node-cache"));
const config_2 = require("./config");
const myCache = new node_cache_1.default({ stdTTL: 86400, checkperiod: 86400 });
var options = {
    method: "POST",
    url: config_2.oauthURL,
    headers: {
        "content-type": "application/json",
    },
    body: JSON.stringify({
        client_id: config_2.client_id,
        client_secret: config_2.client_secret,
        audience: config_2.audience,
        grant_type: config_2.grant_type,
    }),
};
exports.setOrderToDeliverect = (data) => {
    const myToken = myCache.get("cacheToken");
    if (myToken === undefined) {
        request_1.default(options, function (error, response) {
            if (error)
                throw new Error(error);
            var resp = JSON.parse(response.body);
            const obj = {
                access_token: resp.access_token,
                token_type: resp.token_type,
            };
            myCache.set("cacheToken", obj, 86400);
            var option = {
                method: "POST",
                url: `${config_1.DeliverectApi}/${config_1.ChannelName}/order/${data.storeData.channelLinkId}`,
                headers: {
                    "Content-Type": "application/json",
                    //@ts-ignore
                    Authorization: `${resp.token_type} ${resp.access_token}`,
                },
                body: JSON.stringify(data_1.dataOrder(data)),
            };
            request_1.default(option, function (error, response) {
                if (error)
                    throw new Error(error);
                console.log(response.body, "No token");
            });
        });
    }
    else {
        var option = {
            method: "POST",
            url: `${config_1.DeliverectApi}/${config_1.ChannelName}/order/${data.storeData.channelLinkId}`,
            headers: {
                "Content-Type": "application/json",
                //@ts-ignore
                Authorization: `${myToken.token_type} ${myToken.access_token}`,
            },
            body: JSON.stringify(data_1.dataOrder(data)),
        };
        request_1.default(option, function (error, response) {
            if (error)
                throw new Error(error);
            console.log(response.body, "token");
        });
    }
};
