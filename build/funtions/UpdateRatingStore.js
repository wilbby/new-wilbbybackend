"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateRestaurant = void 0;
const RatingCalculartor_1 = require("../funtions/RatingCalculartor");
const rating_1 = __importDefault(require("../models/rating"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
exports.updateRestaurant = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const g = yield restaurant_1.default.findOne({ _id: id });
    const ratings = yield rating_1.default.find({ restaurant: g === null || g === void 0 ? void 0 : g._id });
    const averageRating = RatingCalculartor_1.RatingCalculator(ratings ? ratings : []);
    restaurant_1.default.findOneAndUpdate({ _id: g === null || g === void 0 ? void 0 : g._id }, { rating: averageRating ? `${averageRating.toFixed(1)}` : "0.0" }, () => {
        console.log("done");
    });
});
