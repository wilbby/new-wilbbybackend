import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
  },

  storeId: {
    type: String,
  },

  productId: {
    type: String,
  },

  items: {
    type: mongoose.Schema.Types.Mixed,
  },

  addToCart: {
    type: Boolean,
    default: true,
  },
});

export interface ICart extends Document {
  userId: string;
  storeId: string;
  productId: string;
  items: any;
  addToCart: boolean;
}

export default mongoose.model<ICart>("Newcard", cartSchema);
