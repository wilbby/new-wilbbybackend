"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.testOtter = void 0;
const config_1 = require("./config");
var request = require("request");
var details = {
    client_id: config_1.client_id,
    client_secret: config_1.client_secret,
    grant_type: "client_credentials",
    scope: "ping",
};
let formBody = [];
for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    //@ts-ignore
    formBody.push(encodedKey + "=" + encodedValue);
}
//@ts-ignore
formBody = formBody.join("&");
var options = {
    method: "POST",
    url: config_1.authURL,
    headers: {
        "Content-Type": "application/x-www-form-urlencoded",
    },
    body: formBody,
};
exports.testOtter = () => {
    request(options, function (error, response) {
        console.log(response.statusCode, response.statusMessage, response.body);
    });
};
