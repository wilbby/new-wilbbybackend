import emailSchema from "../models/EmailsDatas";

export const SaveEmailWilbby = (
  email: string,
  name: string,
  lastName: string
) => {
  const newEmail = new emailSchema({
    name: name,
    lastName: lastName,
    email: email,
  });
  newEmail.save();
};
