import request from "request";
import { DeliverectApi, ChannelName } from "./config";
import { dataOrder } from "./data";
import NodeCache from "node-cache";
import {
  oauthURL,
  client_id,
  client_secret,
  audience,
  grant_type,
} from "./config";
const myCache = new NodeCache({ stdTTL: 86400, checkperiod: 86400 });

var options = {
  method: "POST",
  url: oauthURL,
  headers: {
    "content-type": "application/json",
  },
  body: JSON.stringify({
    client_id: client_id,
    client_secret: client_secret,
    audience: audience,
    grant_type: grant_type,
  }),
};

export const setOrderToDeliverect = (data: any) => {
  const myToken = myCache.get("cacheToken");
  if (myToken === undefined) {
    request(options, function (error, response) {
      if (error) throw new Error(error);
      var resp = JSON.parse(response.body);
      const obj = {
        access_token: resp.access_token,
        token_type: resp.token_type,
      };
      myCache.set("cacheToken", obj, 86400);
      var option = {
        method: "POST",
        url: `${DeliverectApi}/${ChannelName}/order/${data.storeData.channelLinkId}`,
        headers: {
          "Content-Type": "application/json",
          //@ts-ignore
          Authorization: `${resp.token_type} ${resp.access_token}`,
        },
        body: JSON.stringify(dataOrder(data)),
      };
      request(option, function (error, response) {
        if (error) throw new Error(error);
        console.log(response.body, "No token");
      });
    });
  } else {
    var option = {
      method: "POST",
      url: `${DeliverectApi}/${ChannelName}/order/${data.storeData.channelLinkId}`,
      headers: {
        "Content-Type": "application/json",
        //@ts-ignore
        Authorization: `${myToken.token_type} ${myToken.access_token}`,
      },
      body: JSON.stringify(dataOrder(data)),
    };
    request(option, function (error, response) {
      if (error) throw new Error(error);
      console.log(response.body, "token");
    });
  }
};
