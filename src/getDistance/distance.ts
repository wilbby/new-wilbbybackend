import orderSchema from "../models/newOrder/order";
var distance = require("google-distance-matrix");

distance.key("AIzaSyDy80slwpOlrhOhU-qLYFmGKN3FWgAsmGA");
distance.units("imperial");

export const setDistanceToOrder = async (OrderID: String) => {
  const order = await orderSchema.findOne({ _id: OrderID });
  if (order?.orderType === "delivery") {
    var origins = [
      `${order?.deliveryAddressData.lat},${order?.deliveryAddressData.lgn}`,
    ];
    var destinations = [
      `${order?.storeData.adress.lat},${order?.storeData.adress.lgn}`,
    ];

    distance.matrix(origins, destinations, function (err: any, distances: any) {
      if (err) {
        return console.log(err);
      }
      if (!distances) {
        return console.log("no distances");
      }
      if (distances.status === "OK") {
        for (var i = 0; i < origins.length; i++) {
          for (var j = 0; j < destinations.length; j++) {
            var origin = distances.origin_addresses[i];
            var destination = distances.destination_addresses[j];
            if (distances.rows[0].elements[j].status == "OK") {
              var distance = distances.rows[i].elements[j].distance.value;

              const dis = distance / 1000;
              const km = dis * 100;
              const propina = order.tip > 0 ? order.tip : 0;

              const totalFinal = 0;
              const kmformat = km.toFixed(0);

              const input = {
                km: Number(kmformat),
                order: order?.channelOrderDisplayId,
                total: totalFinal,
                propina: propina,
              };

              orderSchema.findOneAndUpdate(
                { _id: OrderID },
                {
                  $set: {
                    reportRiders: input,
                  },
                },
                (err, order) => {}
              );
            } else {
              console.log(
                destination + " is not reachable by land from " + origin
              );
            }
          }
        }
      }
    });
  } else {
    null;
  }
};
