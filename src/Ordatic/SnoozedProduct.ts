import Products from "../models/newMenu/products";
import Modifield from "../models/newMenu/modifiers";

export const SnoozedProduct = (data) => {
  if (data.data.type === "item") {
    if (data.eventType === "item-disabled") {
      Products.findOneAndUpdate(
        { plu: data.data.posRef },
        {
          $set: {
            snoozed: true,
          },
        },
        (err, order) => {}
      );
    } else {
      Products.findOneAndUpdate(
        { plu: data.data.posRef },
        {
          $set: {
            snoozed: false,
          },
        },
        (err, order) => {}
      );
    }
  } else {
    if (data.eventType === "item-disabled") {
      Modifield.findOneAndUpdate(
        { plu: data.data.posRef },
        {
          $set: {
            snoozed: true,
          },
        },
        (err, order) => {}
      );
    } else {
      Modifield.findOneAndUpdate(
        { plu: data.data.posRef },
        {
          $set: {
            snoozed: false,
          },
        },
        (err, order) => {}
      );
    }
  }
};
