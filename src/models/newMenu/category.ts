import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const categorySchema = new mongoose.Schema(
  {
    name: { type: String },
    description: { type: String },
    nameTranslations: { type: mongoose.Schema.Types.Mixed },
    descriptionTranslations: { type: mongoose.Schema.Types.Mixed },
    account: { type: String },
    posLocationId: { type: String },
    posCategoryType: { type: String },
    subCategories: { type: [String] },
    posCategoryId: { type: String },
    imageUrl: { type: String },
    products: { type: [String] },
    menu: { type: String },
    snoozed: {type: Boolean, default: false },
    sortedChannelProductIds: { type: [String] },
    subProductSortOrder: { type: [String] },
    subProducts: { type: [String] },
    level: { type: Number },
    availabilities: { type: [mongoose.Schema.Types.Mixed] },
    internalId: { type: String },
    storeId: { type: String },
    sorting: { type: Number },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface ICategoryProduct extends Document {
  name: string;
  description: string;
  nameTranslations: any;
  descriptionTranslations: any;
  account: string;
  posLocationId: string;
  posCategoryType: string;
  subCategories: string[];
  posCategoryId: string;
  imageUrl: string;
  products: string[];
  menu: string;
  sortedChannelProductIds: string[];
  subProductSortOrder: string[];
  subProducts: string[];
  level: number;
  availabilities: any[];
  internalId: string;
  storeId: string;
  sorting: number;
  snoozed: boolean;
}

export default mongoose.model<ICategoryProduct>(
  "categoryProduct",
  categorySchema
);
