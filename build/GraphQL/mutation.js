"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Mutation = void 0;
const categorias_1 = __importDefault(require("../models/categorias"));
const user_1 = __importDefault(require("../models/user"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const Pago_1 = __importDefault(require("../models/Pago"));
const transacciones_1 = __importDefault(require("../models/transacciones"));
const adress_1 = __importDefault(require("../models/adress"));
const userAdmin_1 = __importDefault(require("../models/userAdmin"));
const cupones_1 = __importDefault(require("../models/cupones"));
const Favorito_1 = __importDefault(require("../models/Favorito"));
const riders_1 = __importDefault(require("../models/riders"));
const Status_messages_1 = require("./Status_messages");
const fs_1 = __importDefault(require("fs"));
const mongoose_1 = require("mongoose");
const rating_1 = __importDefault(require("../models/rating"));
const Opinion_1 = __importDefault(require("../models/Opinion"));
const tipo_1 = __importDefault(require("../models/tipo"));
const highkitchenCategory_1 = __importDefault(require("../models/highkitchenCategory"));
const adressStore_1 = __importDefault(require("../models/adressStore"));
const post_1 = __importDefault(require("../models/post"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const mailJet_1 = require("../mailJet");
const saveEmail_1 = require("../SaveEmailList/saveEmail");
const tipotienda_1 = __importDefault(require("../models/tipotienda"));
const custonorder_1 = __importDefault(require("../models/custonorder"));
const quincena_1 = __importDefault(require("../models/quincena"));
const offerts_1 = __importDefault(require("../models/offerts"));
const collections_1 = __importDefault(require("../models/collections"));
const subcollection_1 = __importDefault(require("../models/subcollection"));
const SendEmail_1 = require("../RecoveryPassword/Email/SendEmail");
const cityclose_1 = __importDefault(require("../models/cityclose"));
const trasactionRider_1 = __importDefault(require("../models/trasactionRider"));
const SavedataLogin_1 = require("../funtions/SavedataLogin");
const distance_1 = require("../getDistance/distance");
const dotenv_1 = __importDefault(require("dotenv"));
const LoginEmailSend_1 = require("../RecoveryPassword/Email/LoginEmailSend");
const Notifications_1 = require("./Notifications");
const DeleteHoldedRecip_1 = require("../Holded/DeleteHoldedRecip");
const addToCart_1 = __importDefault(require("../models/newOrder/addToCart"));
const ads_1 = __importDefault(require("../models/ads"));
const aws_sdk_1 = __importDefault(require("aws-sdk"));
//new order
const order_1 = __importDefault(require("../models/newOrder/order"));
const products_1 = __importDefault(require("../models/newMenu/products"));
const Menu_1 = __importDefault(require("../models/newMenu/Menu"));
const bundles_1 = __importDefault(require("../models/newMenu/bundles"));
const modifierGroups_1 = __importDefault(require("../models/newMenu/modifierGroups"));
const modifiers_1 = __importDefault(require("../models/newMenu/modifiers"));
const category_1 = __importDefault(require("../models/newMenu/category"));
const sendNotification_1 = require("../Deliverect/sendNotification");
const createRiderSale_1 = require("../Holded/createRiderSale");
const UpdateRatingStore_1 = require("../funtions/UpdateRatingStore");
const RiderAssign_1 = require("../Otter/RiderAssign");
const sendNotificationCustonOrder_1 = require("../funtions/sendNotificationCustonOrder");
aws_sdk_1.default.config.update({
    region: process.env.region,
    accessKeyId: process.env.accessKeyId,
    secretAccessKey: process.env.secretAccessKey,
});
var s3Bucket = new aws_sdk_1.default.S3({ params: { Bucket: "products-wilbby" } });
const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
dotenv_1.default.config({ path: "variables.env" });
const { ObjectId } = mongoose_1.Types;
const crearToken = (restaurant, secreto, expiresIn) => {
    const { _id } = restaurant;
    return jsonwebtoken_1.default.sign({ _id }, secreto, { expiresIn });
};
exports.Mutation = {
    autenticarRestaurant: (root, { email, password }) => __awaiter(void 0, void 0, void 0, function* () {
        const restaurant = yield restaurant_1.default.findOne({ email });
        if (!restaurant) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.USER_NOT_FOUND,
                data: null,
            };
        }
        const passwordCorrecto = yield bcryptjs_1.default.compare(password, restaurant.password);
        if (!passwordCorrecto) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.INCORRECT_PASSWORD,
                data: null,
            };
        }
        return {
            success: true,
            message: "Bienvenido a My Store by Wilbby",
            data: {
                token: crearToken(restaurant, process.env.SECRETO, "9999 years"),
                id: restaurant._id,
            },
        };
    }),
    autenticarUsuario: (root, { email, password, input }) => __awaiter(void 0, void 0, void 0, function* () {
        const users = yield user_1.default.findOne({ email });
        if (!users) {
            return {
                success: false,
                message: "Aún no de te has registrado en Wilbby",
                data: null,
            };
        }
        const passwordCorrecto = yield bcryptjs_1.default.compare(password, users.password);
        if (!passwordCorrecto) {
            return {
                success: false,
                message: "Contraseña incorrecta",
                data: null,
            };
        }
        else {
            LoginEmailSend_1.LoginEmail(email, input, users);
            SavedataLogin_1.saveDataLogin(users._id, input);
            return {
                success: true,
                message: "Bienvenido a Wilbby",
                data: {
                    token: crearToken(users, process.env.SECRETO, "9999 years"),
                    id: users._id,
                    user: users,
                },
            };
        }
    }),
    autenticarRiders: (root, { email, password }) => __awaiter(void 0, void 0, void 0, function* () {
        const users = yield riders_1.default.findOne({ email });
        if (!users) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.USER_NOT_FOUND,
                data: null,
            };
        }
        const passwordCorrecto = yield bcryptjs_1.default.compare(password, users.password);
        if (!passwordCorrecto) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.INCORRECT_PASSWORD,
                data: null,
            };
        }
        return {
            success: true,
            message: "Bienvenid@ a Wilbby®",
            data: {
                token: crearToken(users, process.env.SECRETO, "9999 years"),
                id: users._id,
            },
        };
    }),
    crearUsuario: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        mailJet_1.SaveEmail(input.email, input.nombre);
        saveEmail_1.SaveEmailWilbby(input.email, input.nombre, input.apellidos);
        SendEmail_1.welcomeEmail(input.email, input.nombre);
        // check if email exists
        const emailExists = yield user_1.default.findOne({ email: input.email });
        if (emailExists) {
            return {
                success: false,
                message: "Ya eres parte de Wilbby",
                data: null,
            };
        }
        const nuevoUsuario = new user_1.default({
            name: input.nombre,
            lastName: input.apellidos,
            email: input.email,
            password: input.password,
            city: input.city,
            termAndConditions: input.termAndConditions,
        });
        nuevoUsuario.id = nuevoUsuario._id;
        return new Promise((resolve, object) => {
            nuevoUsuario.save((error) => __awaiter(void 0, void 0, void 0, function* () {
                if (error)
                    object({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: null,
                    });
                else {
                    resolve({
                        success: true,
                        message: "Usuario agregado con éxito",
                        data: nuevoUsuario,
                    });
                    /*  CratePaypalUser(
                      input.nombre,
                      input.apellidos,
                      input.emai,
                      nuevoUsuario._id
                    ); */
                    yield stripe.customers.create({
                        name: input.nombre,
                        email: input.email,
                        description: "Clientes de Wilbby",
                    }, function (err, customer) {
                        user_1.default.findOneAndUpdate({ _id: nuevoUsuario._id }, {
                            $set: {
                                StripeID: customer.id,
                            },
                        }, (err, customers) => {
                            if (err) {
                            }
                        });
                    });
                }
            }));
        });
    }),
    crearRiders: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        // check if email exists
        const emailExists = yield riders_1.default.findOne({ email: input.email });
        if (emailExists) {
            return {
                success: false,
                message: "Ya eres parte de Wilbby",
                data: null,
            };
        }
        const nuevoUsuario = new riders_1.default({
            name: input.nombre,
            lastName: input.apellidos,
            email: input.email,
            password: input.password,
            termAndConditions: input.termAndConditions,
            telefono: input.telefono,
            isAvalible: input.isAvalible,
            city: input.city,
            contactCode: input.contactCode,
        });
        nuevoUsuario.id = nuevoUsuario._id;
        return new Promise((resolve, object) => {
            nuevoUsuario.save((error) => {
                if (error)
                    object({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: null,
                    });
                else {
                    resolve({
                        success: true,
                        message: "Usuario agregado con éxito",
                        data: nuevoUsuario,
                    });
                }
            });
        });
    }),
    crearAdmin: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        // check if email exists
        const emailExists = yield userAdmin_1.default.findOne({ email: input.email });
        if (emailExists) {
            return {
                success: false,
                message: "Ya eres parte de Wilbby",
                data: null,
            };
        }
        const nuevoUsuario = new userAdmin_1.default({
            name: input.nombre,
            lastName: input.apellidos,
            email: input.email,
            password: input.password,
            termAndConditions: input.termAndConditions,
        });
        nuevoUsuario.id = nuevoUsuario._id;
        return new Promise((resolve, object) => {
            nuevoUsuario.save((error) => {
                if (error)
                    object({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: null,
                    });
                else {
                    resolve({
                        success: true,
                        message: "Usuario agregado con éxito",
                        data: nuevoUsuario,
                    });
                }
            });
        });
    }),
    autenticarAdmin: (root, { email, password }) => __awaiter(void 0, void 0, void 0, function* () {
        const users = yield userAdmin_1.default.findOne({ email });
        if (!users) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.USER_NOT_FOUND,
                data: null,
            };
        }
        const passwordCorrecto = yield bcryptjs_1.default.compare(password, users.password);
        if (!passwordCorrecto) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.INCORRECT_PASSWORD,
                data: null,
            };
        }
        return {
            success: true,
            message: "Bienvenid@ a Wilbby®",
            data: {
                token: crearToken(users, process.env.SECRETO, "9999 years"),
                id: users._id,
            },
        };
    }),
    singleUpload(parent, { file }) {
        const matches = file.match(/^data:.+\/(.+);base64,(.*)$/);
        const ext = matches[1];
        const base64_data = matches[2];
        const buffer = Buffer.from(base64_data, "base64");
        const filename = `${Date.now()}-file.${ext}`;
        const filenameWithPath = `${__dirname}/../../uploads/images/${filename}`;
        return new Promise((resolve, reject) => {
            fs_1.default.writeFile(filenameWithPath, buffer, (error) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve({ filename });
                }
            });
        });
    },
    singleUploadToAws(parent, { file }) {
        const buf = Buffer.from(file.replace(/^data:image\/\w+;base64,/, ""), "base64");
        var data = {
            Key: `${Date.now()}-product-wilbby`,
            Body: buf,
            ContentEncoding: "base64",
            ContentType: "image/jpeg",
            ACL: "public-read",
        };
        return new Promise((resolve, reject) => {
            //@ts-ignore
            s3Bucket.upload(data, function (err, data) {
                if (err) {
                    reject({
                        data: null,
                    });
                }
                else {
                    resolve({
                        data: data,
                    });
                }
            });
        });
    },
    avatarUploadToAws(parent, { file, id }) {
        const buf = Buffer.from(file.replace(/^data:image\/\w+;base64,/, ""), "base64");
        var data = {
            Key: `${Date.now()}-avatar-wilbby`,
            Body: buf,
            ContentEncoding: "base64",
            ContentType: "image/jpeg",
            ACL: "public-read",
        };
        return new Promise((resolve, reject) => {
            //@ts-ignore
            s3Bucket.upload(data, function (err, data) {
                if (err) {
                    reject({
                        data: null,
                    });
                }
                else {
                    user_1.default.findOneAndUpdate({ _id: id }, { avatar: data.Location }, { new: true }, (error, _usuario) => {
                        if (error)
                            reject(error);
                        else {
                            resolve({
                                data: { data, _usuario },
                            });
                        }
                    });
                }
            });
        });
    },
    createCategory: (root, { input }) => {
        const nuevaCategoria = new categorias_1.default({
            title: input.title,
            image: input.image,
            description: input.description,
            sorting: input.sorting,
            excludeCity: input.excludeCity,
            visible: input.visible,
            navigate: input.navigate,
            url: input.url,
        });
        return new Promise((resolve, rejects) => {
            nuevaCategoria.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.USER_ADDED,
                        success: true,
                    });
                }
            });
        });
    },
    createTipo: (root, { input }) => {
        const nuevatipo = new tipo_1.default({
            title: input.title,
            image: input.image,
            description: input.description,
        });
        return new Promise((resolve, rejects) => {
            nuevatipo.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.USER_ADDED,
                        success: true,
                    });
                }
            });
        });
    },
    createHighkitchenCategory: (root, { input }) => {
        const highkitchenCategory = new highkitchenCategory_1.default({
            title: input.title,
            image: input.image,
            description: input.description,
        });
        return new Promise((resolve, rejects) => {
            highkitchenCategory.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.USER_ADDED,
                        success: true,
                    });
                }
            });
        });
    },
    createTipoTienda: (root, { input }) => {
        const nuevatipo = new tipotienda_1.default({
            title: input.title,
            image: input.image,
            description: input.description,
        });
        return new Promise((resolve, rejects) => {
            nuevatipo.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.USER_ADDED,
                        success: true,
                    });
                }
            });
        });
    },
    eliminarTipoTienda: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            tipotienda_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    rejects(error);
                else
                    resolve("Eliminado correctamente");
            });
        });
    },
    eliminarTipo: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            tipo_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    rejects(error);
                else
                    resolve("Eliminado correctamente");
            });
        });
    },
    eliminarCategory: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            categorias_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    rejects(error);
                else
                    resolve("Eliminado correctamente");
            });
        });
    },
    actualizarAdmin: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            userAdmin_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error, _usuario) => {
                if (error)
                    reject(error);
                else
                    resolve(_usuario);
            });
        });
    }),
    actualizarUsuario: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            user_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error, _usuario) => {
                if (error)
                    reject(error);
                else
                    resolve(_usuario);
            });
        });
    }),
    actualizarRiders: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            riders_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error, _usuario) => {
                if (error)
                    reject(error);
                else
                    resolve(_usuario);
            });
        });
    }),
    eliminarAdmin: (root, { id }) => {
        return new Promise((resolve, reject) => {
            userAdmin_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else {
                    resolve({
                        messages: "Usuario eliminado con éxito",
                        success: true,
                    });
                }
            });
        });
    },
    eliminarUsuario: (root, { id }) => {
        return new Promise((resolve, reject) => {
            user_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else {
                    resolve({
                        messages: "Usuario eliminado con éxito",
                        success: true,
                    });
                }
            });
        });
    },
    eliminarRiders: (root, { id }) => {
        return new Promise((resolve, reject) => {
            riders_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Usuario eliminado con éxito",
                        success: true,
                    });
            });
        });
    },
    createRestaurant: (root, { input }) => {
        const nuevoRestaurant = new restaurant_1.default({
            title: input.title,
            image: input.image,
            description: input.description,
            categoryName: input.categoryName,
            categoryID: input.categoryID,
            minime: input.minime,
            shipping: input.shipping,
            extras: input.extras,
            phone: input.phone,
            autoshipping: input.autoshipping,
            alegeno_url: input.alegeno_url,
            email: input.email,
            logo: input.logo,
            inOffert: input.inOffert,
            previous_shipping: input.previous_shipping,
            tipo: input.tipo,
            password: input.password,
            includeCity: input.includeCity,
            type: input.type,
            open: input.open,
            isnew: input.isnew,
            llevar: input.llevar,
            highkitchen: input.highkitchen,
            stimateTime: input.stimateTime,
            slug: input.slug,
            ispartners: input.ispartners,
            schedule: input.schedule,
            channelLinkId: input.channelLinkId,
            collections: input.collections,
            isDeliverectPartner: input.isDeliverectPartner,
            adress: input.adress,
            city: input.city,
            contactCode: input.contactCode,
            socialLink: input.socialLink,
            salvingPack: input.salvingPack,
            scheduleOnly: input.scheduleOnly,
            noScheduled: input.noScheduled,
            shorting: input.shorting,
        });
        return new Promise((resolve, rejects) => {
            nuevoRestaurant.save((error, res) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    eliminarRestaurant: (root, { id }) => {
        return new Promise((resolve, reject) => {
            restaurant_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Restaurante eliminado con éxito",
                        success: true,
                    });
            });
        });
    },
    actualizarRestaurant: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            restaurant_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un error con tu solicitud vuelve a intentalo por favor",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Datos actualizado con éxito",
                        success: true,
                    });
            });
        });
    }),
    crearFavorito: (root, { restaurantID, usuarioId }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        const favorito = new Favorito_1.default({
            usuarioId,
            restaurantID,
        });
        return new Promise((resolve, reject) => {
            favorito.save((error) => {
                if (error) {
                    reject({
                        messages: "Hay un problema con tu solicitud",
                        success: false,
                    });
                }
                else {
                    resolve({
                        messages: "Farmacia añadido a favorito",
                        success: true,
                    });
                }
            });
        });
    }),
    eliminarFavorito: (root, { id, userID }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            Favorito_1.default.findOneAndDelete({ restaurantID: id, usuarioId: userID }, (error, data) => {
                console.log(error);
                if (error)
                    reject({
                        messages: "Hay un problema con tu solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Restaurante eliminado de favorito",
                        success: true,
                    });
            });
        });
    }),
    createProduct: (root, { input }) => {
        input.data.price = Math.round(input.data.price);
        const newProduct = new products_1.default(input.data);
        return new Promise((resolve, rejects) => {
            newProduct.save((error, product) => {
                console.log(newProduct._id, newProduct.name);
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    createBundled: (root, { input }) => {
        const newBundled = new bundles_1.default(input.data);
        return new Promise((resolve, rejects) => {
            newBundled.save((error, bundled) => {
                console.log(bundled);
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    createModifieldGroup: (root, { input }) => {
        const newmodifieldGroup = new modifierGroups_1.default(input.data);
        return new Promise((resolve, rejects) => {
            newmodifieldGroup.save((error, bundled) => {
                console.log(bundled);
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    createModifield: (root, { input }) => {
        input.data.price = Math.round(input.data.price);
        const newmodifield = new modifiers_1.default(input.data);
        return new Promise((resolve, rejects) => {
            newmodifield.save((error, bundled) => {
                console.error(bundled._id, bundled.name);
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    crearCupon: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const nuevoCupon = new cupones_1.default({
                clave: input.clave,
                descuento: input.descuento,
                tipo: input.tipo,
                usage: input.usage,
                expire: input.expire,
                exprirable: input.exprirable,
                user: input.user,
                description: input.description,
                private: input.private,
                uniqueStore: input.uniqueStore,
                store: input.store,
                city: input.city,
            });
            return new Promise((resolve, reject) => {
                nuevoCupon.save((error, cupon) => __awaiter(void 0, void 0, void 0, function* () {
                    if (error) {
                        return reject(error);
                    }
                    else {
                        if (input.private) {
                            const user = yield user_1.default.findOne({ _id: input.user });
                            sendNotification_1.sendNotification(user === null || user === void 0 ? void 0 : user.OnesignalID, input.description);
                        }
                        return resolve(cupon);
                    }
                }));
            });
        }
        catch (error) {
            return new Promise((_resolve, reject) => {
                return reject({
                    success: false,
                    message: "Hay un problema con su solicitud",
                    data: null,
                });
            });
        }
    }),
    eliminarCupon: (root, { id }) => {
        return new Promise((resolve, reject) => {
            cupones_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                    });
                else
                    resolve({
                        success: true,
                        message: "Miembro eliminado con éxito",
                    });
            });
        });
    },
    crearValoracion: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const nuevoValoracion = new rating_1.default({
                user: input.user,
                comment: input.comment,
                value: input.value,
                restaurant: input.restaurant,
            });
            return new Promise((resolve, reject) => {
                nuevoValoracion.save((error, rating) => {
                    if (error) {
                        return reject(error);
                    }
                    else {
                        UpdateRatingStore_1.updateRestaurant(input.restaurant);
                        return resolve(rating);
                    }
                });
            });
        }
        catch (error) {
            return new Promise((_resolve, reject) => {
                return reject({
                    success: false,
                    message: "Hay un problema con su solicitud",
                    data: null,
                });
            });
        }
    }),
    createOpinion: (root, { input }) => {
        const nuevaOpinion = new Opinion_1.default({
            plato: input.plato,
            comment: input.comment,
            rating: input.rating,
            user: input.user,
        });
        return new Promise((resolve, rejects) => {
            nuevaOpinion.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    crearPago: (root, { input }) => {
        const nuevoPago = new Pago_1.default({
            nombre: input.nombre,
            iban: input.iban,
            restaurantID: input.restaurantID,
        });
        nuevoPago.id = nuevoPago._id;
        return new Promise((resolve, reject) => {
            nuevoPago.save((error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Pago añadido con éxito",
                        success: true,
                    });
            });
        });
    },
    eliminarPago: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            Pago_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Pago eliminado con éxito",
                        success: true,
                    });
            });
        });
    }),
    crearDeposito: (root, { input }) => {
        const nuevoDeposito = new transacciones_1.default({
            fecha: new Date(),
            estado: input.estado,
            total: input.total,
            restaurantID: input.restaurantID,
        });
        nuevoDeposito.id = nuevoDeposito._id;
        return new Promise((resolve, reject) => {
            nuevoDeposito.save((error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Deposito añadido con éxito",
                        success: true,
                    });
            });
        });
    },
    eliminarDeposito: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            transacciones_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Hay un problema con su solicitud",
                    });
                else
                    resolve({
                        success: true,
                        messages: "Deposito eliminado con éxito",
                    });
            });
        });
    }),
    createAdress: (root, { input }) => {
        const nuevaadress = new adress_1.default({
            formatted_address: input.formatted_address,
            puertaPiso: input.puertaPiso,
            type: input.type,
            usuario: input.usuario,
            city: input.city,
            postalcode: input.postalcode,
            lat: input.lat === "null" ? "39.1582846" : input.lat,
            lgn: input.lgn === "null" ? "-3.0215836" : input.lgn,
        });
        return new Promise((resolve, rejects) => {
            nuevaadress.save((error, adress) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: adress,
                    });
                }
            });
        });
    },
    createStoreAdress: (root, { input }) => {
        const nuevaadress = new adressStore_1.default({
            calle: input.calle,
            numero: input.numero,
            codigoPostal: input.codigoPostal,
            ciudad: input.ciudad,
            store: input.store,
            lat: input.lat,
            lgn: input.lgn,
        });
        return new Promise((resolve, rejects) => {
            nuevaadress.save((error, adress) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        success: true,
                        message: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        data: adress,
                    });
                }
            });
        });
    },
    actualizarAdress: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            adress_1.default.findOneAndUpdate({ _id: input.id }, input, { new: true }, (error, adress) => {
                if (error)
                    reject(error);
                else
                    resolve({
                        messages: "Dirección actualizada con éxito",
                        success: true,
                        data: adress,
                    });
            });
        });
    }),
    eliminarAdress: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            adress_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Dirección eliminado con éxito",
                        success: true,
                    });
            });
        });
    }),
    eliminarAdressStore: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            adressStore_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Dirección eliminado con éxito",
                        success: true,
                    });
            });
        });
    }),
    eliminarPost: (root, { id }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        const isAdmin = yield userAdmin_1.default.findOne({ _id: usuarioActual._id });
        if (!isAdmin) {
            return {
                success: true,
                messages: "No estas autorizado para eliminar un post",
                data: null,
            };
        }
        else {
            return new Promise((resolve, reject) => {
                post_1.default.findOneAndDelete({ _id: id }, (error) => {
                    if (error)
                        reject({
                            message: "Hay un problema con su solicitud",
                            success: false,
                        });
                    else
                        resolve({
                            messages: "Dirección eliminado con éxito",
                            success: true,
                        });
                });
            });
        }
    }),
    actualizarPost: (root, { input }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        const isAdmin = yield userAdmin_1.default.findOne({ _id: usuarioActual._id });
        if (!isAdmin) {
            return {
                success: true,
                messages: "No estas autorizado para actualizar un post",
                data: null,
            };
        }
        else {
            return new Promise((resolve, reject) => {
                post_1.default.findOneAndUpdate({ _id: input.id }, input, { new: true }, (error, post) => {
                    if (error)
                        reject(error);
                    else
                        resolve({
                            success: true,
                            message: "Dirección actualizada con éxito",
                            data: post,
                        });
                });
            });
        }
    }),
    createPost: (root, { input }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        const isAdmin = yield userAdmin_1.default.findOne({ _id: usuarioActual._id });
        if (!isAdmin) {
            return {
                success: true,
                message: "No estas autorizado para crear un post",
                data: null,
            };
        }
        else {
            const nuevoPost = new post_1.default({
                title: input.title,
                image: input.image,
                shortDescription: input.shortDescription,
                like: input.like,
                tags: input.tags,
                author: input.author,
                category: input.category,
                readTime: input.readTime,
                content: input.content,
                slug: input.slug,
                country: input.country,
            });
            return new Promise((resolve, rejects) => {
                nuevoPost.save((error, post) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        resolve({
                            success: true,
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            data: post,
                        });
                    }
                });
            });
        }
    }),
    createCustonOrder: (root, { input }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        const isAdmin = yield user_1.default.findOne({ _id: usuarioActual._id });
        if (!isAdmin) {
            return {
                success: true,
                messages: "No estas autorizado para crear un esta orden",
                data: null,
            };
        }
        else {
            const nuevaOrder = new custonorder_1.default({
                display_id: input.display_id,
                riders: input.riders,
                origin: input.origin,
                destination: input.destination,
                schedule: input.schedule,
                distance: input.distance,
                nota: input.nota,
                date: input.date,
                city: input.city,
                userID: input.userID,
                estado: input.estado,
                status: input.status,
                progreso: input.progreso,
                total: input.total,
                product_stimate_price: input.product_stimate_price,
            });
            return new Promise((resolve, rejects) => {
                nuevaOrder.save((error, order) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        resolve({
                            success: true,
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            data: order,
                        });
                    }
                });
            });
        }
    }),
    actualizarOrderProcess: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            custonorder_1.default.findOneAndUpdate({ _id: input.id }, input, { new: true }, (error, order) => {
                if (error) {
                    reject(error);
                }
                else {
                    sendNotificationCustonOrder_1.SendNotificationCustomOrder(order);
                    resolve({
                        success: true,
                        messages: "Orden actualizada",
                        data: order,
                    });
                }
            });
        });
    }),
    actualizarCity: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            cityclose_1.default.findOneAndUpdate({ _id: input.id }, input, { new: true }, (error) => {
                if (error)
                    reject({
                        messages: "Algo va mal intentalo de Nuevo",
                        success: true,
                    });
                else
                    resolve({
                        messages: "Dirección actualizada con éxito",
                        success: true,
                    });
            });
        });
    }),
    createCity: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevaCity = new cityclose_1.default({
            close: input.close,
            city: input.city,
            title: input.title,
            subtitle: input.subtitle,
            imagen: input.imagen,
        });
        return new Promise((resolve, rejects) => {
            nuevaCity.save((error, order) => {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    createOffert: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevaOffert = new offerts_1.default({
            store: input.store,
            slug: input.slug,
            city: input.city,
            apertura: input.apertura,
            cierre: input.cierre,
            imagen: input.imagen,
            open: input.open,
        });
        return new Promise((resolve, rejects) => {
            nuevaOffert.save((error, order) => {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    eliminarOfferts: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            offerts_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Algo va mal intentalo de Nuevo",
                        success: true,
                    });
                else
                    resolve({
                        messages: "Oferta eliminada con éxito",
                        success: true,
                    });
            });
        });
    }),
    createTransactionRider: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevatransaction = new trasactionRider_1.default({
            rider: input.rider,
            km: input.km,
            order: input.order,
            total: input.total,
            propina: input.propina,
            iva: input.iva,
        });
        return new Promise((resolve, rejects) => {
            nuevatransaction.save((error, order) => __awaiter(void 0, void 0, void 0, function* () {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    const riders = yield riders_1.default.findOne({ _id: input.rider });
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            }));
        });
    }),
    createQuincena: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevaquincena = new quincena_1.default({
            fromDate: input.fromDate,
            toDate: input.toDate,
            numberQuincena: input.numberQuincena,
        });
        return new Promise((resolve, rejects) => {
            nuevaquincena.save((error) => {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    createCollection: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevaCollection = new collections_1.default({
            title: input.title,
            image: input.image,
            store: input.store,
            sorting: input.sorting,
        });
        return new Promise((resolve, rejects) => {
            nuevaCollection.save((error) => {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    createsubCollection: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevasubCollection = new subcollection_1.default({
            title: input.title,
            collectiontype: input.collectiontype,
            sorting: input.sorting,
        });
        return new Promise((resolve, rejects) => {
            nuevasubCollection.save((error) => {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    addToCart: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const newCart = new addToCart_1.default({
            userId: input.userId,
            storeId: input.storeId,
            productId: input.productId,
            items: input.items,
            addToCart: input.addToCart,
        });
        const exist = yield addToCart_1.default.findOne({ productId: input.productId });
        if (exist) {
            return new Promise((resolve, rejects) => {
                addToCart_1.default
                    .deleteMany({ productId: input.productId })
                    .then(function () {
                    newCart.save((error) => {
                        if (error) {
                            rejects({
                                messages: "Algo va mal intentalo de nuevo",
                                success: true,
                            });
                        }
                        else {
                            resolve({
                                messages: "Producto añadido a la cesta",
                                success: true,
                            });
                        }
                    });
                })
                    .catch(function (error) {
                    console.log(error); // Failure
                });
                /*  newCartSchema.findOneAndUpdate(
                  { productId: input.productId },
                  input,
                  { new: true },
                  (error: any) => {
                    if (error) {
                      rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                      });
                    } else {
                      resolve({
                        messages: "Producto añadido a la cesta",
                        success: true,
                      });
                    }
                  }
                ); */
            });
        }
        else {
            return new Promise((resolve, rejects) => {
                newCart.save((error) => {
                    if (error) {
                        rejects({
                            messages: "Algo va mal intentalo de nuevo",
                            success: true,
                        });
                    }
                    else {
                        resolve({
                            messages: "Producto añadido a la cesta",
                            success: true,
                        });
                    }
                });
            });
        }
    }),
    deleteCartItem: (root, { id, fromWeb }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para comtinuar",
                success: false,
            };
        }
        let condition = {};
        if (fromWeb) {
            condition = { productId: id };
        }
        else {
            condition = { _id: id };
        }
        return new Promise((resolve, reject) => {
            addToCart_1.default.findOneAndDelete(condition, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con tu solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Producto eliminado de la cesta",
                        success: true,
                    });
            });
        });
    }),
    crearModificarNewOrden: (root, { input }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (!usuarioActual) {
                return new Promise((_resolve, reject) => {
                    return _resolve({
                        success: false,
                        message: "Debes iniciar sesión para continuar",
                        data: null,
                    });
                });
            }
            let courrier;
            courrier = yield riders_1.default.findOne({
                _id: input.courier ? input.courier : null,
            });
            let orden;
            if (input._id) {
                orden = yield order_1.default.findById(ObjectId(input._id)).exec();
                orden.set({
                    courier: input.courier ? input.courier : null,
                    courierData: courrier,
                });
            }
            if (!input.store) {
                const restaurants = yield restaurant_1.default
                    .findById(ObjectId(input.store))
                    .exec();
                if (!restaurants) {
                    return new Promise((_resolve, reject) => {
                        return _resolve({
                            success: false,
                            message: "Restaurante no disponible",
                            data: null,
                        });
                    });
                }
            }
            let cupon;
            if (!!input.clave) {
                const cup = yield cupones_1.default.findOne({ clave: input.clave }).exec();
                const existOrder = yield order_1.default.find({
                    customer: input.customer,
                    cupon: cup === null || cup === void 0 ? void 0 : cup._id,
                    status: { $ne: "Pendiente de pago" },
                });
                if (!cup) {
                    return new Promise((_resolve, reject) => {
                        return _resolve({
                            success: false,
                            message: "Este cupón no existe",
                            data: null,
                        });
                    });
                }
                else if (existOrder.length >= cup.usage) {
                    return new Promise((_resolve, reject) => {
                        return _resolve({
                            success: false,
                            message: "Ya haz utilizado este cupón prueba con otro código",
                            data: null,
                        });
                    });
                }
                else if (cup.exprirable) {
                    if (cup.expire < new Date()) {
                        return new Promise((_resolve, reject) => {
                            return _resolve({
                                success: false,
                                message: "Este cupòn ya ha expirado",
                                data: null,
                            });
                        });
                    }
                    else {
                        cupon = cup;
                    }
                }
                else {
                    cupon = cup;
                }
            }
            else if (!!input.cupon) {
                cupon = input.cupon;
            }
            let userData;
            userData = yield user_1.default.findOne({
                _id: input.customer,
            });
            let AdreesData;
            if (input.deliveryAddress) {
                AdreesData = yield adress_1.default.findOne({
                    _id: input.deliveryAddress,
                });
            }
            else {
                AdreesData = yield adress_1.default.findOne({
                    usuario: input.customer,
                });
            }
            if (input.orderType === "delivery" && !AdreesData) {
                return new Promise((_resolve, reject) => {
                    return _resolve({
                        success: false,
                        message: "Debes añadir una dirección para continuar",
                        data: null,
                    });
                });
            }
            let StoreData;
            StoreData = yield restaurant_1.default.findOne({
                _id: input.store,
            });
            let product;
            product = yield addToCart_1.default.find({
                userId: input.customer,
                storeId: input.store,
            });
            if (!product) {
                return new Promise((_resolve, reject) => {
                    return _resolve({
                        success: false,
                        message: "No hay productos para este pedido",
                        data: null,
                    });
                });
            }
            let discuntTotal = 0;
            if (cupon) {
                const deTo = (input.payment * cupon.descuento) / 100;
                switch (cupon.tipo) {
                    case "dinero":
                        discuntTotal = cupon.descuento;
                        break;
                    case "porcentaje":
                        discuntTotal = deTo;
                        break;
                }
            }
            if (!orden || !orden._id) {
                orden = new order_1.default({
                    cupon: cupon && cupon._id ? cupon._id : cupon,
                    channelOrderDisplayId: input.channelOrderDisplayId,
                    orderType: input.orderType,
                    pickupTime: input.pickupTime,
                    estimatedPickupTime: input.estimatedPickupTime,
                    deliveryTime: input.deliveryTime,
                    courier: input.courier,
                    courierData: courrier,
                    statusProcess: [],
                    customerData: userData,
                    customer: input.customer,
                    store: input.store,
                    storeData: StoreData,
                    deliveryAddressData: AdreesData,
                    deliveryAddress: input.deliveryAddress,
                    payment: input.payment,
                    note: input.note,
                    items: input.items ? input.items : product,
                    tip: input.tip,
                    numberOfCustomers: input.numberOfCustomers,
                    deliveryCost: input.deliveryCost,
                    serviceCharge: input.serviceCharge,
                    discountTotal: Math.round(discuntTotal),
                    IntegerValue: input.IntegerValue,
                    Needcutlery: input.Needcutlery,
                    scheduled: input.scheduled,
                });
            }
            else {
                if (!orden.cupon) {
                    orden.set({ cupon: cupon ? cupon._id : null });
                }
            }
            return new Promise((resolve, reject) => {
                orden.save((error, ordenguardada) => {
                    if (error) {
                        return resolve({
                            success: false,
                            message: "Algo salio mal intentalo de nuevo",
                            data: null,
                        });
                    }
                    else {
                        if (!ordenguardada.descuento && cupon && cupon._id) {
                            ordenguardada.descuento = cupon;
                        }
                        distance_1.setDistanceToOrder(ordenguardada._id);
                        if (input.courier && StoreData.isOtterPartner) {
                            RiderAssign_1.UpdateOrderRider(ordenguardada);
                        }
                        return resolve({
                            success: true,
                            message: "Todo ha salido bien",
                            data: ordenguardada,
                        });
                    }
                });
            });
        }
        catch (error) {
            return new Promise((_resolve, reject) => {
                return reject({
                    success: false,
                    message: "Algo salio mal intentalo de nuevo",
                    data: null,
                });
            });
        }
    }),
    NewOrdenProceed: (root, { ordenId, status, IntegerValue, statusProcess }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            order_1.default.findOneAndUpdate({ _id: ordenId }, {
                status: status,
                IntegerValue: Number(IntegerValue),
                $push: { statusProcess: statusProcess },
            }, { upsert: true }, 
            //@ts-ignore
            (error, order) => __awaiter(void 0, void 0, void 0, function* () {
                if (error) {
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                }
                else {
                    Notifications_1.pushNotificationOrderProccess(status, order);
                    if (status === "Devuelto") {
                        DeleteHoldedRecip_1.deleteRecipt(order);
                    }
                    resolve({
                        messages: "Orden procesada con éxito",
                        success: true,
                    });
                }
            }));
        });
    }),
    snoozedProduct: (root, { id, snoozed }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para continiar",
                success: false,
            };
        }
        return new Promise((resolve, reject) => {
            products_1.default.findOneAndUpdate({ _id: id }, { snoozed: snoozed }, { upsert: true }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Producto marcado como agotado",
                        success: true,
                    });
            });
        });
    }),
    snoozedBundled: (root, { id, snoozed }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para continiar",
                success: false,
            };
        }
        return new Promise((resolve, reject) => {
            bundles_1.default.findOneAndUpdate({ _id: id }, { snoozed: snoozed }, { upsert: true }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Modificador marcado como agotado",
                        success: true,
                    });
            });
        });
    }),
    snoozedModifield: (root, { id, snoozed }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para continiar",
                success: false,
            };
        }
        return new Promise((resolve, reject) => {
            modifiers_1.default.findOneAndUpdate({ _id: id }, { snoozed: snoozed }, { upsert: true }, (error, data) => {
                console.log(data);
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Modificador marcado como agotado",
                        success: true,
                    });
            });
        });
    }),
    snoozedModifieldGroup: (root, { id, snoozed }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        console.log(id, snoozed);
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para continiar",
                success: false,
            };
        }
        return new Promise((resolve, reject) => {
            modifierGroups_1.default.findOneAndUpdate({ _id: id }, { snoozed: snoozed }, { upsert: true }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Modificador marcado como agotado",
                        success: true,
                    });
            });
        });
    }),
    createMenu: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevaMenu = new Menu_1.default(input.data);
        return new Promise((resolve, rejects) => {
            nuevaMenu.save((error) => {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    createNewCategory: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const nuevaCategory = new category_1.default(input.data);
        return new Promise((resolve, rejects) => {
            nuevaCategory.save((error) => {
                if (error) {
                    rejects({
                        messages: "Algo va mal intentalo de nuevo",
                        success: true,
                    });
                }
                else {
                    resolve({
                        messages: "Datos guardado con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    RiderAccepOrder: (root, { id, riderAcceptOrder }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            order_1.default.findOneAndUpdate({ _id: id }, {
                riderAcceptOrder: riderAcceptOrder,
            }, { upsert: true }, 
            //@ts-ignore
            (error, order) => __awaiter(void 0, void 0, void 0, function* () {
                if (error) {
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                }
                else {
                    createRiderSale_1.setToHolded(order.courierData, order.reportRiders.total, order.channelOrderDisplayId);
                    resolve({
                        messages: "Orden procesada con éxito",
                        success: true,
                    });
                }
            }));
        });
    }),
    SendOffertOrder: (root, { id, input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            order_1.default.findOneAndUpdate({ _id: id }, {
                $push: { offertFromRider: input },
            }, { upsert: true }, 
            //@ts-ignore
            (error, order) => __awaiter(void 0, void 0, void 0, function* () {
                if (error) {
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                }
                else {
                    resolve({
                        messages: "Oferta enviada con éxito",
                        success: true,
                    });
                }
            }));
        });
    }),
    eliminarProducto: (root, { id }) => {
        return new Promise((resolve, reject) => {
            products_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else {
                    resolve({
                        messages: "Producto eliminado con éxito",
                        success: true,
                    });
                }
            });
        });
    },
    actualizarProduct: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            products_1.default.findOneAndUpdate({ _id: input.data._id }, input.data, { new: true }, (error) => {
                if (error)
                    reject(error);
                else
                    resolve({
                        messages: "Producto Actualizado",
                        success: true,
                    });
            });
        });
    }),
    actualizarCategoria: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            category_1.default.findOneAndUpdate({ _id: input.data._id }, input.data, { new: true }, (error, datos) => {
                if (error) {
                    reject(error);
                }
                else {
                    if (input.data.snoozed && datos) {
                        datos &&
                            datos.products.forEach((x) => __awaiter(void 0, void 0, void 0, function* () {
                                const prod = yield products_1.default.findOne({ _id: x });
                                products_1.default.findOneAndUpdate({ _id: prod === null || prod === void 0 ? void 0 : prod._id }, {
                                    snoozed: true,
                                }, { upsert: true }, () => {
                                    console.log("done");
                                });
                            }));
                    }
                    else {
                        datos &&
                            datos.products.forEach((x) => __awaiter(void 0, void 0, void 0, function* () {
                                const prod = yield products_1.default.findOne({ _id: x });
                                products_1.default.findOneAndUpdate({ _id: prod === null || prod === void 0 ? void 0 : prod._id }, {
                                    snoozed: false,
                                }, { upsert: true }, () => {
                                    console.log("done");
                                });
                            }));
                    }
                    resolve({
                        messages: "Producto Actualizado",
                        success: true,
                    });
                }
            });
        });
    }),
    eliminarCategoria: (root, { id }) => {
        return new Promise((resolve, reject) => {
            category_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else {
                    resolve({
                        messages: "Producto eliminado con éxito",
                        success: true,
                    });
                }
            });
        });
    },
    createCardSource: (root, { customers, paymentMethod }) => {
        return new Promise((resolve, reject) => __awaiter(void 0, void 0, void 0, function* () {
            stripe.paymentMethods
                .attach(paymentMethod, {
                customer: customers,
            })
                .then((res) => {
                resolve({
                    messages: "Tarjeta añadida al wallet",
                    success: true,
                });
            })
                .catch((e) => {
                reject({
                    messages: "Algo salio mal intentalo de nuevo",
                    success: false,
                });
            });
        }));
    },
    createAds: (root, { data }) => {
        const nuevaAds = new ads_1.default(data.data);
        return new Promise((resolve, rejects) => {
            nuevaAds.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.USER_ADDED,
                        success: true,
                    });
                }
            });
        });
    },
    eliminarAds: (root, { id }) => {
        return new Promise((resolve, reject) => {
            ads_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else {
                    resolve({
                        messages: "Producto eliminado con éxito",
                        success: true,
                    });
                }
            });
        });
    },
};
