"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.welcomeRiderEmail = void 0;
const emailRider_1 = __importDefault(require("./emailRider"));
const riders_1 = __importDefault(require("../models/riders"));
const crypto_1 = __importDefault(require("crypto"));
const nodemailer_1 = __importDefault(require("nodemailer"));
exports.welcomeRiderEmail = (email) => {
    const token = crypto_1.default.randomBytes(20).toString("hex");
    var newvalues = { $set: { forgotPasswordToken: token } };
    const transporter = nodemailer_1.default.createTransport({
        host: "smtp.gmail.com",
        service: "gmail",
        port: 465,
        secure: true,
        auth: {
            user: process.env.EMAIL_ADDRESS,
            pass: process.env.EMAIL_PASSWORD,
        },
    });
    const mailOptions = {
        from: process.env.EMAIL_ADDRESS,
        to: email,
        subject: `ACTIVAR CUENTA WILBBY - Bienvenido a Wilbby para Riders`,
        text: `ACTIVAR CUENTA WILBBY - Bienvenido a Wilbby para Riders`,
        html: emailRider_1.default(token, email),
    };
    transporter.sendMail(mailOptions, (err) => {
        if (err) {
            console.log("Error al enviar");
        }
        else {
            riders_1.default.findOneAndUpdate({ email: email }, newvalues, {
                //options
                new: true,
                strict: false,
                useFindAndModify: false,
            }, (err, updated) => { });
        }
    });
};
