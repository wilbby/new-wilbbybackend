import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const citySchema = new mongoose.Schema({
  close: {
    type: Boolean,
  },
  city: {
    type: String,
  },
  title: {
    type: String,
  },

  subtitle: {
    type: String,
  },

  imagen: {
    type: String,
  },
});

export interface ICity extends Document {
  title: string;
  image: String;
  description: string;
}

export default mongoose.model<ICity>("cities", citySchema);
