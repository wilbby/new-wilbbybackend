"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const MenuSchema = new mongoose_1.default.Schema({
    title: {
        type: String,
        text: true,
        required: true,
    },
    subtitle: {
        type: String,
    },
    restaurant: {
        type: String,
        required: true,
    },
    sorting: {
        type: Number,
        default: 0,
    },
    deliverectID: {
        type: String,
    },
    products: {
        type: [String],
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("menu", MenuSchema);
