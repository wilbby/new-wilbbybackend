"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const welcomeRiders_1 = require("./welcomeRiders");
class EmailRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/send-email-welcome-riders", (req, res) => {
            const { email } = req.query;
            welcomeRiders_1.welcomeRiderEmail(email);
            res
                .status(200)
                .json({ message: "Email enviado con exito", success: true })
                .end();
        });
    }
}
const emailRouter = new EmailRouter();
emailRouter.routes();
exports.default = emailRouter.router;
