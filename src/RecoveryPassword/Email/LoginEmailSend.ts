const nodemailer = require("nodemailer");
import LoginEmails from "./LoginEmail";

export const LoginEmail = (email: string, data: any, user: any) => {
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    service: "gmail",
    port: 465,
    secure: true,
    auth: {
      user: process.env.EMAIL_ADDRESS,
      pass: process.env.EMAIL_PASSWORD,
    },
  });
  const mailOptions = {
    from: process.env.EMAIL_ADDRESS,
    to: email ? email : "info@wilbby.com",
    subject: "Nuevo inicio de sesión en tu cuenta",
    text: "Nuevo inicio de sesión en tu cuenta",
    html: LoginEmails(data, user),
  };

  transporter.sendMail(mailOptions);
};
