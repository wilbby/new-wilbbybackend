"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.registerWebHookURL = void 0;
const config_1 = require("./config");
var request = require("request");
exports.registerWebHookURL = (order, data) => {
    const input = {
        eventId: data.externalIdentifiers.id,
        eventTime: order === null || order === void 0 ? void 0 : order.created_at,
        eventType: "orders.order_status_update",
        metadata: {
            storeId: data.storeId,
            applicationId: config_1.client_id,
            resourceHref: `${config_1.orderURL}/${data.externalIdentifiers.id}`,
            resourceId: data.externalIdentifiers.id,
            payload: {
                orderStatusHistory: [
                    {
                        status: "RECIVED_ORDER",
                        eventTime: new Date(),
                    },
                ],
            },
        },
    };
    const options = {
        method: "POST",
        url: config_1.WEB_HOOK_URL,
        headers: {
            "Content-Type": "application/json",
            Authorization: `MAC ${config_1.Webhook_Secret}`,
            "User-Agent": config_1.user_Agent,
        },
        body: JSON.stringify(input),
    };
    request(options, function (error, response) {
        console.log(response.body);
    });
};
