import { Types } from "mongoose";
import ordenSchema, { IOrders } from "../models/newOrder/order";
import riderSchema, { IRider } from "../models/riders";
import { sendNotification } from "../GraphQL/sendNotificationRider";
import { sendNotificationCustomer } from "../GraphQL/sendNotificationCustomer";
import { ordenUpdate } from "../Deliverect/ordenUpdate";

const { ObjectId } = Types;

export const AsingRider = async (orde: IOrders) => {
  const isRider = () => {
    if (orde.storeData.city === "Tomelloso") {
      return "611f847ef0668e8ae9c80605";
    } else if (orde.storeData.city === "Burgos") {
      return "5fb7de125fd35b9c72ce2423";
    } else if (orde.storeData.city === "Alcázar de San Juan") {
      return "611f83cdf0668e8ae9c80604";
    }
  };
  //@ts-ignore
  const rider: IRider = await riderSchema.findOne({
    _id: isRider(),
  });

  ordenSchema.findOneAndUpdate(
    { _id: orde._id },
    {
      courier: ObjectId(rider._id),
      courierData: rider,
    },
    { upsert: true },
    (err, order) => {
      if (err) {
        console.log(">>>", err);
      } else {
        if (!orde.storeData.ispartners) {
          ordenUpdate(orde._id, "Listo para recoger", 70);
          setTimeout(() => {
            sendNotification(
              rider.OnesignalID,
              "Tienes el pedido listo para recoger ✌️ ⚡️",
              "Listo para recoger",
              true
            );
          }, 30000);
        }
        sendNotification(
          rider.OnesignalID,
          "Has recibido un nuevo pedido a por todas ✌️ ⚡️",
          "Has recibido un nuevo pedido",
          true
        );
        sendNotificationCustomer(
          orde.customerData.OnesignalID,
          `Te hemos asignado a ${rider.name} para entregarte el pedido`,
          "Repartidor asignado"
        );
      }
    }
  );
};
