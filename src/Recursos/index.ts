import Order from "../models/newOrder/order";
import Cart from "../models/newOrder/addToCart";
import Store from "../models/restaurant";
import productSchema from "../models/newMenu/products";
import plato from "../models/nemu/platos";
import categoria from "../models/nemu/menu";
import CatSchema from "../models/restaurant";
import collectionSchema from "../models/collections";
import categorySchema from "../models/categorias";
import { RatingCalculator } from "../funtions/RatingCalculartor";
import ratingSchema from "../models/rating";

import { AsingRider } from "../funtions/asingRiders";

import { storeFormat, setHorario } from "../storeFormat";
import restaurant from "../models/restaurant";

import product from "../models/newMenu/products";

import category, { ICategoryProduct } from "../models/newMenu/category";
import bundles from "../models/newMenu/bundles";

export const RecursosUpdate = (): void => {
  //new restaurant(storeFormat()).save();

  const updateMainCategoria = async () => {
    const categorys = await categorySchema.find();

    categorys.forEach((x) => {
      categorySchema.updateMany(
        { _id: x },
        {
          excludeCity: [],
          visible: true,
          navigate: false,
          url: "",
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  //updateMainCategoria();

  const setCategoryDuplicate = async () => {
    const cat = await category.find({ account: "60faf41f0ea294528074a625" });

    cat.forEach(async (x: ICategoryProduct, i: number) => {
      const newCat = new category({
        name: x.name,
        description: x.description,
        account: "610003c7fb1a92e312078816",
        posLocationId: x.posCategoryId,
        posCategoryType: x.posCategoryType,
        subCategories: [],
        posCategoryId: "",
        imageUrl: "",
        products: x.products,
        menu: "610003c7fb1a92e312078816",
        storeId: "610003c7fb1a92e312078816",
        sortedChannelProductIds: [],
        subProductSortOrder: [],
        subProducts: [],
        level: 2,
        sorting: x.sorting,
        availabilities: [],
      });

      newCat.save().then(() => {
        console.log("save");
      });
    });
  };

  const updateProductStoreID = async () => {
    const g = await product.find({ account: "615f53f30cc9d274ab4304d0" });

    g.forEach((x) => {
      product.updateMany(
        { _id: x },
        {
          //@ts-ignore
          storeId: ["60faf41f0ea294528074a625", "610003c7fb1a92e312078816"],
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  const updateOrders = async () => {
    const g = await Order.find({ status: "Listo para recoger" });
    //@ts-ignore
    g.forEach(async (x) => {
      const st = { status: "Entregada", date: new Date() };
      Order.findOneAndUpdate(
        { _id: x?._id },
        {
          status: "Entregada",
          $push: { statusProcess: st },
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  const updateProducVariables = async () => {
    const products = await product.find({
      account: "61a7a1bbc5cc0874468cc368",
    });

    //@ts-ignore
    products.forEach(async (x) => {
      product.findOneAndUpdate(
        { _id: x?._id },
        {
          //@ts-ignore
          subProducts: ["61b899a18585936d282b35f4"],
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  //updateProducVariables();

  const updateProducBundled = async () => {
    const g = await bundles.findOne({ _id: "615f53f30cc9d274ab4304d0" });

    g &&
      g.subProducts.forEach(async (x) => {
        const prod = await product.findOne({ _id: x });
        product.findOneAndUpdate(
          { _id: prod?._id },
          {
            multiply: 2,
            multiMax: 2,
            max: 2,
            min: 2,
          },
          { upsert: true },
          () => {
            console.log("done");
          }
        );
      });
  };

  //updateProducBundled();

  const updateCollection = async () => {
    const g = await collectionSchema.find({
      store: "5fbd841981fafb0aa4c06479",
    });

    g.forEach((x) => {
      collectionSchema.updateMany(
        { _id: x._id },
        {
          store: [
            "5fbd841981fafb0aa4c06479",
            "619a9322c7c9ee895da8dc21",
            "619a918ac7c9ee895da8dc20",
          ],
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  //updateCollection();

  const updateRestaurant = async () => {
    const g = await restaurant.find({ city: "Alcázar de San Juan" });

    g.forEach(async (x, i) => {
      const ratings = await ratingSchema.find({ restaurant: x._id });
      const averageRating = RatingCalculator(ratings ? ratings : []);
      restaurant.findOneAndUpdate(
        { _id: x._id },
        { rating: `${averageRating.toFixed(1)}` },
        () => {
          console.log("done");
        }
      );
    });
  };

  const updateRestaurantOne = async () => {
    restaurant.updateMany(
      { _id: "60fc33acfbf3fe598d4c2a36" },
      setHorario(),
      { upsert: true },
      () => {
        console.log("done");
      }
    );
  };

  //updateRestaurantOne();

  const updateProd = async () => {
    const g = await product.find({ account: "610003c7fb1a92e312078816" });
    g.forEach((x) => {
      const price = (x.price * 10) / 100;
      product.updateMany(
        { _id: x._id },
        {
          price: x.previous_price,
          previous_price: 0,
          offert: false,
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  //updateProd();

  //updateRestaurant()

  //updateCollection()

  //update();

  /*  Order.deleteMany({ status: "Pendiente de pago" }).then(() => {
    console.log("hecho");
  }); */

  /* product.deleteMany({ account: "613b4b5e63541c8ba258a160" }).then(() => {
  console.log("hecho");
}); */

  /* category.deleteMany({ account: "60faf41f0ea294528074a602" }).then(() => {
  console.log("hecho");
}); */

  const setCategoryFronViejas = async () => {
    const cat = await categoria.find({
      restaurant: "5ff1d57185c1c068931a3860",
    });

    cat.forEach(async (x, i) => {
      const pro = await plato.find({ menu: x._id });

      const sub = pro.map((y) => {
        //@ts-ignore
        return y._id;
      });
      const newCat = new CatSchema({
        name: x.title,
        description: "",
        account: "5ff1d57185c1c068931a3860",
        posLocationId: "",
        posCategoryType: "",
        subCategories: [],
        posCategoryId: "",
        imageUrl: "",
        products: sub,
        menu: "60be7ee0395b91df865649d5",
        storeId: "5ff1d57185c1c068931a3860",
        sortedChannelProductIds: [],
        subProductSortOrder: [],
        subProducts: [],
        level: 2,
        sorting: i,
        availabilities: [],
      });

      newCat.save().then(() => {
        console.log("save");
      });
    });
  };

  /* setOrders().then(() => {
  console.log("guardado");
}); */

  const setMenu = async () => {
    const data = await plato.find({
      restaurant: "5fe4a2f5a55336531bb12a51",
    });

    data.forEach(async (x, i) => {
      //@ts-ignore
      const Pric = Number(x.price) * 100;

      const pri = Pric.toFixed(0);
      const plato = {
        _id: x._id,
        //@ts-ignore
        name: x.title,
        //@ts-ignore
        description: x.ingredientes,
        nameTranslations: {},
        descriptionTranslations: {},
        //@ts-ignore
        account: x.restaurant,
        //@ts-ignore
        location: x.restaurant,
        productType: 1,
        plu: "",
        price: Number(pri),
        previous_price: null,
        priceLevels: {},
        sortOrder: i,
        deliveryTax: 0,
        takeawayTax: 0,
        multiply: 1,
        multiMax: 1,
        posProductId: "",
        posProductCategoryId: [],
        subProducts: [],
        productTags: [],
        posCategoryIds: [],
        //@ts-ignore
        imageUrl: x.imagen,
        max: 1,
        min: 1,
        capacityUsages: [],
        //@ts-ignore
        parentId: x.menu,
        visible: true,
        snoozed: false,
        subProductSortOrder: [],
        internalId: x._id,
        new: false,
        popular: false,
        offert: false,
        recomended: false,
        quantity: 1,
        storeId: "5fe4a2f5a55336531bb12a51",
      };

      new productSchema(plato).save().then(() => {
        console.log("done");
      });
    });
  };

  //setMenu()

  //console.log(new Date());
};
