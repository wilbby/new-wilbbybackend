import { Request, Response, Router } from "express";
import paypal from "paypal-rest-sdk";
import dotenv from "dotenv";
import orderSchema, { IOrders } from "../models/newOrder/order";
import cartSchema from "../models/newOrder/addToCart";
import custonoderSchema from "../models/custonorder";
import { setOrderToDeliverect } from "../Deliverect/Deliverect";
import { pushOrderTopOrdatic } from "../Ordatic/api";
import { pushOrderToOtter } from "../Otter/api";
import { setOrderToHolded } from "../Holded/Apis";
import { sendNotification } from "./sendNotificationToStore";
import { scheduleTime } from "../funtions/scheduleTime";
import { getMariaHorario } from "../funtions/ScheduledMaria";
import { AsingRider } from "../funtions/asingRiders";
import { AsingRiderCustonOrder } from "../funtions/asingRidersCustonOrder";
import { Types } from "mongoose";
dotenv.config({ path: "variables.env" });

const { ObjectId } = Types;

paypal.configure({
  mode: "live", //sandbox or live
  client_id: process.env.PAYPALCLIENTID || "",
  client_secret: process.env.PAYPALCLIENTSECRET || "",
});

class PaypalRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/paypal", (req: Request, res: Response) => {
      const { price, orderid, userId, currency } = req.query;
      let create_payment_json = {
        intent: "sale",
        payer: {
          payment_method: "paypal",
        },
        redirect_urls: {
          return_url: `https://api.v1.wilbby.com/success?price=${price}&orderid=${orderid}&userId=${userId}&currency=${currency}`,
          cancel_url: "https://api.v1.wilbby.com/cancel",
        },
        transactions: [
          {
            amount: {
              currency: currency ? currency : "EUR",
              total: req.query.price,
            },
            description: "Peinado & Perales SL (Wilbby)",
          },
        ],
      };
      // @ts-ignore
      paypal.payment.create(create_payment_json, function (error, payment) {
        console.log(error, payment);
        if (error) {
          res.redirect("/cancel");
        } else {
          // @ts-ignore
          res.redirect(payment.links[1].href);
        }
      });
    });

    this.router.get("/success", (req: Request, res: Response) => {
      var PayerID = req.query.PayerID;
      var paymentId = req.query.paymentId;
      const { price, orderid, userId, currency } = req.query;
      var execute_payment_json = {
        payer_id: PayerID,
        transactions: [
          {
            amount: {
              currency: currency ? currency : "EUR",
              total: price,
            },
          },
        ],
      };
      // @ts-ignore
      paypal.payment.execute(
        // @ts-ignore
        paymentId,
        execute_payment_json,
        function (error, payment) {
          if (error) {
            res.redirect("/cancel");
          }
          if (payment) {
            orderSchema.findOneAndUpdate(
              //@ts-ignore
              { _id: ObjectId(orderid) },
              {
                status: "Nueva",
                IntegerValue: 30,
                paymentMethod: "Paypal",
                pagoPaypal: payment,
                orderIsAlreadyPaid: true,
                //@ts-ignore
                $push: {
                  statusProcess: { status: "Nueva", date: new Date() },
                },
              },
              { upsert: true },
              // @ts-ignore
              async (err, order: IOrders) => {
                if (!err) {
                  if (order?.storeData.isDeliverectPartner) {
                    setOrderToDeliverect(order);
                  } else if (order?.storeData.isOrdaticPartner) {
                    pushOrderTopOrdatic(order);
                  } else if (order?.storeData.isOtterPartner) {
                    pushOrderToOtter(order);
                  } else {
                    sendNotification(
                      order?.storeData.OnesignalID,
                      "Has recibido un nuevo pedido a por todas 🌮 🌯 🫔 🥗 🥘"
                    );
                  }

                  if (
                    scheduleTime(getMariaHorario().schedule) &&
                    order.orderType === "delivery"
                  ) {
                    AsingRider(order);
                  }

                  setOrderToHolded(order);
                }
                cartSchema
                  .deleteMany({ userId: String(userId) })
                  .then(function () {
                    console.log("Data deleted cart"); // Success
                  })
                  .catch(function (error) {
                    console.log(error); // Failure
                  });
                res.render("success");
              }
            );
            console.log("Get Payment Response");
          }
        }
      );
    });
    this.router.get("/cancel", (req: Request, res: Response) => {
      res.render("cancel");
    });

    this.router.get("/paypal-custom-order", (req: Request, res: Response) => {
      const { price, order, currency } = req.query;
      let create_payment_json = {
        intent: "sale",
        payer: {
          payment_method: "paypal",
        },
        redirect_urls: {
          return_url: `https://api.v1.wilbby.com/success-custom?price=${price}&order=${order}&currency=${currency}`,
          cancel_url: "https://api.v1.wilbby.com/cancel-custom",
        },
        transactions: [
          {
            amount: {
              currency: currency ? currency : "EUR",
              total: req.query.price,
            },
            description: "Peinado & Perales SL (Wilbby)",
          },
        ],
      };
      // @ts-ignore
      paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
          res.render("cancel");
        } else {
          res.render("cancel");
          // @ts-ignore
          res.redirect(payment.links[1].href);
        }
      });
    });

    this.router.get("/success-custom", (req: Request, res: Response) => {
      var PayerID = req.query.PayerID;
      var paymentId = req.query.paymentId;
      const { price, order, currency } = req.query;
      var execute_payment_json = {
        payer_id: PayerID,
        transactions: [
          {
            amount: {
              currency: currency ? currency : "EUR",
              total: price,
            },
          },
        ],
      };
      // @ts-ignore
      paypal.payment.execute(
        // @ts-ignore
        paymentId,
        execute_payment_json,
        function (error, payment) {
          if (error) {
            res.render("cancel");
          }
          if (payment) {
            custonoderSchema.findOneAndUpdate(
              { _id: order },
              {
                estado: "Pagado",
                progreso: "25",
                status: "active",
                pagoPaypal: payment,
              },
              (err, orde: any) => {
                if (orde) {
                  if (scheduleTime(getMariaHorario().schedule)) {
                    AsingRiderCustonOrder(orde);
                  }
                  res.render("success");
                }
              }
            );
          }
        }
      );
    });
    this.router.get("/cancel-custom", (req: Request, res: Response) => {
      res.render("cancel");
    });
  }
}

const paypalRouter = new PaypalRouter();
paypalRouter.routes();

export default paypalRouter.router;
