"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Subscription = void 0;
const graphql_subscriptions_1 = require("graphql-subscriptions");
const pubsub = new graphql_subscriptions_1.PubSub();
const SOMETHING_CHANGED_TOPIC = "something_changed";
exports.Subscription = {
    somethingChanged: {
        subscribe: () => pubsub.asyncIterator(SOMETHING_CHANGED_TOPIC),
    },
};
