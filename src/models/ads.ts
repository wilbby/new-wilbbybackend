import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const adsSchema = new mongoose.Schema({
  name: {
    type: String,
    text: true,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  sorting: { type: Number },
  includeCity: { type: [String] },
  visible: { type: Boolean },
  navigate: { type: Boolean },
  url: { type: String },
  email: { type: String },
  category: { type: String },
  isHome: { type: Boolean },
  dataEmail: { type: mongoose.Schema.Types.Mixed },
});

export interface IAds extends Document {
  name: string;
  image: String;
  sorting: number;
  includeCity: string[];
  visible: boolean;
  navigate: boolean;
  url: string;
  category: string;
  isHome: boolean;
  dataEmail: any;
}

export default mongoose.model<IAds>("ads", adsSchema);
