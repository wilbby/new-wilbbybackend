"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const restaurant_1 = __importDefault(require("../models/restaurant"));
const Menu_1 = require("./Menu");
const SnoozedProduct_1 = require("./SnoozedProduct");
const OrderUpdate_1 = require("./OrderUpdate");
class OrdaticRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.post("/api/ordaticEvents", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const data = req.body;
            const store = yield restaurant_1.default.findOne({
                isOrdaticPartnerID: data.storeId,
            });
            if (data.eventType === "catalog-updated") {
                Menu_1.setMenuFromOrdatic(data, store);
            }
            else if (data.eventType === "item-disabled" || "item-enabled") {
                SnoozedProduct_1.SnoozedProduct(data);
            }
            else {
                OrderUpdate_1.OrderUpdate(data);
            }
            res.status(200).end();
        }));
    }
}
const ordaticRouter = new OrdaticRouter();
ordaticRouter.routes();
exports.default = ordaticRouter.router;
