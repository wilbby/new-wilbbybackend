import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const pagoSchema = new mongoose.Schema(
  {
    nombre: {
      type: String,
    },
    iban: {
      type: String,
    },

    restaurantID: {
      type: String,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IPago extends Document {
  nombre: string;
  iban: String;
  restaurantID: number;
}

export default mongoose.model<IPago>("pago", pagoSchema);
