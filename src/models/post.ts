import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const postSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      text: true,
      unique: true,
    },
    image: {
      type: String,
      required: true,
      text: true,
    },
    shortDescription: {
      type: String,
      required: true,
      unique: true,
    },

    like: {
      type: Number,
      default: 0,
    },

    tags: {
      type: [String],
    },

    author: {
      type: String,
    },

    category: {
      type: String,
    },

    readTime: {
      type: String,
    },

    content: {
      type: String,
    },

    slug: {
      type: String,
    },

    country: {
      type: String,
    },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IPost extends Document {
  title: string;
  image: String;
  shortDescription: string;
}

export default mongoose.model<IPost>("post", postSchema);
