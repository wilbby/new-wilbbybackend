import { IOrders } from "../models/newOrder/order";
import { authURL, client_id, client_secret, orderURL } from "./config";
import { data } from "./data";
import { registerWebHookURL } from "./Webhook-register";
var request = require("request");

var details = {
  client_id: client_id,
  client_secret: client_secret,
  grant_type: "client_credentials",
  scope: "orders.create",
};

let formBody = [];
for (var property in details) {
  var encodedKey = encodeURIComponent(property);
  var encodedValue = encodeURIComponent(details[property]);
  //@ts-ignore
  formBody.push(encodedKey + "=" + encodedValue);
}
//@ts-ignore
formBody = formBody.join("&");

var options = {
  method: "POST",
  url: authURL,
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
  },
  body: formBody,
};

export const pushOrderToOtter = (order: IOrders) => {
  request(options, function (error, response) {
    const auth = JSON.parse(response.body);
    var option = {
      method: "POST",
      url: orderURL,
      headers: {
        "Content-Type": "application/json",
        "X-Application-Id": client_id,
        "X-Store-Id": order.storeData.OtterPartnerId,
        Authorization: `Bearer ${auth.access_token}`,
      },
      body: JSON.stringify(data(order)),
    };

    request(option, function (error, response: any) {
      const res = JSON.parse(response.body);
      //@ts-ignore
      registerWebHookURL(order, res);
    });
  });
};
