import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const MenuSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      text: true,
      required: true,
    },

    subtitle: {
      type: String,
    },

    restaurant: {
      type: String,
      required: true,
    },

    sorting: {
      type: Number,
      default: 0,
    },

    deliverectID: {
      type: String,
    },

    products: {
      type: [String],
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IMenu extends Document {
  title: string;
  subtitle: string;
  restaurant?: string;
  sorting: number;
  products: string[];
}

export default mongoose.model<IMenu>("menu", MenuSchema);
