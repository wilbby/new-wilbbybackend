import { Response, Request, NextFunction } from "express";
import jwt from "jsonwebtoken";

interface IPayload {
  _id: string;
}

export const TokenValidations = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.header("auth-token");

  if (!token) {
    res.status(401).json({
      success: false,
      message: "No tienes suficiententes permisos para acceder",
    });
  }

  const payload = jwt.verify(
    token || "noToken",
    process.env.SECRETO || "secrettoken"
  ) as IPayload;

  req.userId = payload._id;
  next();
};
