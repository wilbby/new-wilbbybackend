"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenValidations = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
exports.TokenValidations = (req, res, next) => {
    const token = req.header("auth-token");
    if (!token) {
        res.status(401).json({
            success: false,
            message: "No tienes suficiententes permisos para acceder",
        });
    }
    const payload = jsonwebtoken_1.default.verify(token || "noToken", process.env.SECRETO || "secrettoken");
    req.userId = payload._id;
    next();
};
