"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const transSchema = new mongoose_1.default.Schema({
    estado: {
        type: String,
    },
    total: {
        type: String,
    },
    fecha: {
        type: Date,
    },
    restaurantID: {
        type: String,
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("transacciones", transSchema);
