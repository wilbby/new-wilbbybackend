"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.dataRider = exports.dataUpdate = exports.data = void 0;
const moment_1 = __importDefault(require("moment"));
exports.data = (order) => {
    const date = order.scheduled
        ? //@ts-ignore
            moment_1.default(order.deliveryTime).format()
        : order.created_at;
    const extras = order.tip + order.deliveryCost + order.serviceCharge;
    const subTotal = order.payment - extras;
    const items = order.items.map((element) => {
        const SubItem = element.items.subItems.map((elements) => {
            const sub = elements && elements.subItems ? elements.subItems : [];
            const su = sub.map((elemento) => {
                const item2 = {
                    id: elemento._id,
                    name: elemento.name,
                    quantity: elemento.quantity,
                    price: elemento.price / 100,
                    groupName: "",
                    groupId: elemento.parentId,
                };
                return item2;
            });
            const item = {
                id: elements._id,
                name: elements.name,
                quantity: elements.quantity,
                price: elements.price / 100,
                groupName: "",
                groupId: element.parentId,
                modifiers: su,
            };
            if (su.length === 0) {
                delete item.modifiers;
            }
            return item;
        });
        const item = {
            id: element.items._id,
            name: element.items.name,
            quantity: element.items.quantity,
            note: "",
            categoryId: element.items.parentId,
            categoryName: "",
            price: element.items.price / 100,
            modifiers: SubItem,
        };
        if (SubItem.length === 0) {
            delete item.modifiers;
        }
        return item;
    });
    return {
        externalIdentifiers: {
            id: order._id,
            friendlyId: order.channelOrderDisplayId,
        },
        items: items,
        orderedAt: order.created_at,
        currencyCode: "EUR",
        customer: {
            name: order.customerData.name,
            phone: order.customerData.telefono,
            email: order.customerData.email,
        },
        customerNote: order.note || order.Needcutlery ? "(Incluir cubiertos)" : "",
        status: "NEW_ORDER",
        deliveryInfo: {
            courier: {
                name: order.courierData ? order.courierData.name : "Wilbby",
                phone: order.courierData
                    ? order.courierData.telefono
                    : "+34 947 023 072",
                email: order.courierData ? order.courierData.email : "info@wilbby.com",
            },
            destination: {
                postalCode: order.deliveryAddressData.postalcode,
                city: order.deliveryAddressData.city,
                state: order.deliveryAddressData.city,
                countryCode: "ES",
                addressLines: [order.deliveryAddressData.formatted_address],
                location: {
                    latitude: order.deliveryAddressData.lat,
                    longitude: order.deliveryAddressData.lgn,
                },
            },
            licensePlate: "",
            makeModel: "",
            lastKnownLocation: {
                latitude: Number.parseFloat(order.deliveryAddressData.lat),
                longitude: Number.parseFloat(order.deliveryAddressData.lgn),
            },
            note: "",
        },
        orderTotal: {
            subtotal: subTotal / 100,
            discount: order.discountTotal / 100,
            tax: 0,
            tip: order.tip / 100,
            deliveryFee: order.deliveryCost / 100,
            total: order.payment / 100,
            couponCode: "",
        },
        customerPayments: [
            {
                value: order.payment / 100,
                processingStatus: "PROCESSED",
                paymentMethod: "CARD",
            },
        ],
        fulfillmentInfo: {
            pickupTime: date,
            deliveryTime: date,
            fulfillmentMode: order.orderType === "delivery"
                ? order.storeData.autoshipping
                    ? "RESTAURANT_DELIVERY"
                    : "DELIVERY"
                : "PICKUP",
            schedulingType: order.scheduled ? "FIXED_TIME" : "ASAP",
            courierStatus: "COURIER_ASSIGNED",
        },
    };
};
exports.dataUpdate = (status) => {
    return {
        orderStatus: status,
    };
};
exports.dataRider = (order) => {
    return {
        deliveryInfo: {
            courier: {
                name: order.courierData.name,
                phone: order.courierData.telefono,
                email: order.courierData.email,
            },
            destination: {
                postalCode: order.deliveryAddressData.postalcode,
                city: order.deliveryAddressData.city,
                state: order.deliveryAddressData.city,
                countryCode: "ES",
                addressLines: [order.deliveryAddressData.formatted_address],
                location: {
                    latitude: order.deliveryAddressData.lat,
                    longitude: order.deliveryAddressData.lgn,
                },
            },
            licensePlate: "",
            makeModel: "",
            lastKnownLocation: {
                latitude: Number.parseFloat(order.deliveryAddressData.lat),
                longitude: Number.parseFloat(order.deliveryAddressData.lgn),
            },
            note: "",
        },
    };
};
