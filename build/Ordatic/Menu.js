"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setMenuFromOrdatic = void 0;
const Menu_1 = __importDefault(require("../models/newMenu/Menu"));
const category_1 = __importDefault(require("../models/newMenu/category"));
const products_1 = __importDefault(require("../models/newMenu/products"));
const modifierGroups_1 = __importDefault(require("../models/newMenu/modifierGroups"));
const modifiers_1 = __importDefault(require("../models/newMenu/modifiers"));
const bundles_1 = __importDefault(require("../models/newMenu/bundles"));
exports.setMenuFromOrdatic = (data, store) => {
    data.data.catalogs.forEach((element) => {
        const newMenuInput = {
            menu: element.name,
            menuId: "",
            description: element.desc,
            menuImageURL: "",
            menuType: 1,
            availabilities: [],
            snoozedProducts: [],
            productTags: [],
            currency: 1,
            validations: [],
            nestedModifiers: false,
            channelLinkId: data.storeId,
            storeId: store === null || store === void 0 ? void 0 : store._id,
        };
        const newMenu = new Menu_1.default(newMenuInput);
        Menu_1.default.deleteMany({ storeId: store === null || store === void 0 ? void 0 : store._id })
            .then(function () {
            newMenu.menuId = newMenu._id;
            newMenu.save();
        })
            .catch(function (error) {
            console.log(error); // Failure
        });
        element.sections.forEach((category, i) => __awaiter(void 0, void 0, void 0, function* () {
            const dataCategory = {
                name: category.name,
                description: "",
                nameTranslations: {},
                descriptionTranslations: {},
                account: store === null || store === void 0 ? void 0 : store._id,
                posLocationId: store === null || store === void 0 ? void 0 : store._id,
                posCategoryType: store === null || store === void 0 ? void 0 : store._id,
                subCategories: [],
                posCategoryId: store === null || store === void 0 ? void 0 : store._id,
                imageUrl: "",
                products: category.itemsInnerIds,
                menu: newMenu._id,
                sortedChannelProductIds: [],
                subProductSortOrder: [],
                subProducts: [],
                level: 1,
                availabilities: [],
                internalId: category.innerId,
                storeId: store === null || store === void 0 ? void 0 : store._id,
                sorting: i,
            };
            const newCategory = new category_1.default(dataCategory);
            category_1.default.deleteMany({ account: store === null || store === void 0 ? void 0 : store._id })
                .then(function () {
                newCategory.save();
            })
                .catch(function (error) {
                console.log(error); // Failure
            });
        }));
        element.items.forEach((product) => __awaiter(void 0, void 0, void 0, function* () {
            const dataProduct = {
                name: product.name,
                description: product.desc,
                nameTranslations: {},
                descriptionTranslations: {},
                account: store === null || store === void 0 ? void 0 : store._id,
                location: store === null || store === void 0 ? void 0 : store._id,
                productType: 1,
                plu: product.posRef,
                price: product.price,
                previous_price: 0,
                priceLevels: {},
                sortOrder: 1,
                deliveryTax: 0,
                takeawayTax: 0,
                multiply: 1,
                multiMax: 1,
                posProductId: store === null || store === void 0 ? void 0 : store._id,
                posProductCategoryId: [],
                subProducts: product.modifierGroupsInnerIds,
                productTags: product.sectionsInnerIds,
                posCategoryIds: [],
                imageUrl: product.images[0] ? product.images[0].url : "",
                max: 1,
                min: 1,
                capacityUsages: [],
                parentId: "",
                visible: true,
                snoozed: false,
                subProductSortOrder: [],
                internalId: product.innerId,
                new: false,
                popular: false,
                offert: false,
                recomended: false,
                quantity: 1,
                storeId: store === null || store === void 0 ? void 0 : store._id,
            };
            products_1.default.deleteMany({ storeId: store === null || store === void 0 ? void 0 : store._id })
                .then(function () {
                return __awaiter(this, void 0, void 0, function* () {
                    const newProduct = new products_1.default(dataProduct);
                    const cat = yield category_1.default.find({
                        //@ts-ignore
                        internalId: newProduct.productTags.map((x) => {
                            return x;
                        }),
                    });
                    cat.forEach((x) => {
                        newProduct.parentId = x._id;
                    });
                    newProduct.save();
                });
            })
                .catch(function (error) {
                console.log(error); // Failure
            });
        }));
        element.modifierGroups.forEach((bundled) => __awaiter(void 0, void 0, void 0, function* () {
            const bundledDate = {
                name: bundled.title,
                description: "",
                nameTranslations: {},
                descriptionTranslations: {},
                account: store === null || store === void 0 ? void 0 : store._id,
                location: store === null || store === void 0 ? void 0 : store._id,
                productType: 1,
                plu: bundled.posRef,
                price: 0,
                priceLevels: {},
                sortOrder: 1,
                deliveryTax: 0,
                takeawayTax: 0,
                multiply: bundled.max,
                posProductId: "",
                posProductCategoryId: bundled.modifiersInnerIds,
                subProducts: [],
                productTags: bundled.itemsInnerIds,
                posCategoryIds: [],
                imageUrl: "",
                max: bundled.max,
                min: bundled.min,
                multiMax: bundled.max,
                capacityUsages: [],
                parentId: bundled.innerId,
                snoozed: false,
                subProductSortOrder: [],
                internalId: bundled.innerId,
                recomended: false,
                quantity: 1,
            };
            if (bundled.min > 0) {
                bundles_1.default.deleteMany({ account: store === null || store === void 0 ? void 0 : store._id })
                    .then(function () {
                    return __awaiter(this, void 0, void 0, function* () {
                        const newBundled = new bundles_1.default(bundledDate);
                        newBundled.save().then(() => {
                            console.log("Save Bundled");
                        });
                    });
                })
                    .catch(function (error) {
                    console.log(error); // Failure
                });
            }
            else {
                modifierGroups_1.default.deleteMany({ account: store === null || store === void 0 ? void 0 : store._id })
                    .then(function () {
                    return __awaiter(this, void 0, void 0, function* () {
                        const newBundled = new modifierGroups_1.default(bundledDate);
                        newBundled.save().then(() => {
                            console.log("Save Modifield Group");
                        });
                    });
                })
                    .catch(function (error) {
                    console.log(error); // Failure
                });
            }
        }));
        element.modifiers.forEach((modifield) => __awaiter(void 0, void 0, void 0, function* () {
            const dataProduct = {
                name: modifield.name,
                description: "",
                nameTranslations: {},
                descriptionTranslations: {},
                account: store === null || store === void 0 ? void 0 : store._id,
                location: store === null || store === void 0 ? void 0 : store._id,
                productType: 1,
                plu: modifield.posRef,
                price: modifield.price,
                previous_price: 0,
                priceLevels: {},
                sortOrder: 1,
                deliveryTax: 0,
                takeawayTax: 0,
                multiply: 1,
                multiMax: 1,
                posProductId: store === null || store === void 0 ? void 0 : store._id,
                posProductCategoryId: modifield.modifierGroupsInnerIds,
                subProducts: [],
                productTags: modifield.modifierGroupsInnerIds,
                posCategoryIds: [],
                imageUrl: "",
                max: 1,
                min: 1,
                capacityUsages: [],
                parentId: modifield.innerId,
                visible: true,
                snoozed: false,
                subProductSortOrder: [],
                internalId: modifield.innerId,
                new: false,
                popular: false,
                offert: false,
                recomended: false,
                quantity: 1,
                storeId: store === null || store === void 0 ? void 0 : store._id,
            };
            modifiers_1.default.deleteMany({ account: store === null || store === void 0 ? void 0 : store._id })
                .then(function () {
                return __awaiter(this, void 0, void 0, function* () {
                    const newProduct = new modifiers_1.default(dataProduct);
                    newProduct.save();
                });
            })
                .catch(function (error) {
                console.log(error); // Failure
            });
            products_1.default.deleteMany({ account: store === null || store === void 0 ? void 0 : store._id })
                .then(function () {
                return __awaiter(this, void 0, void 0, function* () {
                    const newProduct = new products_1.default(dataProduct);
                    newProduct.save();
                });
            })
                .catch(function (error) {
                console.log(error); // Failure
            });
        }));
    });
};
