import categoriaSchema from "../models/categorias";
import userSchema from "../models/user";
import restaurantSchema from "../models/restaurant";
import pagoSchema from "../models/Pago";
import transSchema from "../models/transacciones";
import adressSchema from "../models/adress";
import userAdminSchema from "../models/userAdmin";
import Cupones from "../models/cupones";
import favoritoSchema from "../models/Favorito";
import ridersSchema from "../models/riders";
import { STATUS_MESSAGES } from "./Status_messages";
import fs from "fs";
import { Types } from "mongoose";
import RatingSchema from "../models/rating";
import OpinionSchema from "../models/Opinion";
import tipoSchema from "../models/tipo";
import highkitchenCategorySchema from "../models/highkitchenCategory";
import adressStoreSchema from "../models/adressStore";
import postSchema from "../models/post";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { SaveEmail } from "../mailJet";
import { SaveEmailWilbby } from "../SaveEmailList/saveEmail";
import tipotiendaSchema from "../models/tipotienda";
import customOrderSchema from "../models/custonorder";
import quincenaSchama from "../models/quincena";
import offertsSchema from "../models/offerts";
import collectionSchema from "../models/collections";
import subCollectionSchema from "../models/subcollection";
import { welcomeEmail } from "../RecoveryPassword/Email/SendEmail";
import citySchema from "../models/cityclose";
import transactionSchema from "../models/trasactionRider";
import { saveDataLogin } from "../funtions/SavedataLogin";
import { setDistanceToOrder } from "../getDistance/distance";
import dotenv from "dotenv";
import { LoginEmail } from "../RecoveryPassword/Email/LoginEmailSend";
import { pushNotificationOrderProccess } from "./Notifications";
import { deleteRecipt } from "../Holded/DeleteHoldedRecip";
import newCartSchema from "../models/newOrder/addToCart";
import adsSchema from "../models/ads";
import AWS from "aws-sdk";

//new order
import newOrderSchema, { IOrders } from "../models/newOrder/order";
import productSchema from "../models/newMenu/products";
import menuSchema from "../models/newMenu/Menu";
import bundledSchema from "../models/newMenu/bundles";
import modifieldGroup from "../models/newMenu/modifierGroups";
import modifielSchema from "../models/newMenu/modifiers";
import categorySchema, { ICategoryProduct } from "../models/newMenu/category";
import { sendNotification } from "../Deliverect/sendNotification";
import { setToHolded } from "../Holded/createRiderSale";
import { updateRestaurant } from "../funtions/UpdateRatingStore";
import { UpdateOrderRider } from "../Otter/RiderAssign";
import { SendNotificationCustomOrder } from "../funtions/sendNotificationCustonOrder";

AWS.config.update({
  region: process.env.region,
  accessKeyId: process.env.accessKeyId,
  secretAccessKey: process.env.secretAccessKey,
});
var s3Bucket = new AWS.S3({ params: { Bucket: "products-wilbby" } });

const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);

dotenv.config({ path: "variables.env" });

const { ObjectId } = Types;

export interface IRestaurant extends Document {
  email: string;
  password: string;
}

const crearToken = (restaurant: any, secreto: any, expiresIn: any) => {
  const { _id } = restaurant;

  return jwt.sign({ _id }, secreto, { expiresIn });
};

export const Mutation = {
  autenticarRestaurant: async (root: any, { email, password }) => {
    const restaurant = await restaurantSchema.findOne({ email });
    if (!restaurant) {
      return {
        success: false,
        message: STATUS_MESSAGES.USER_NOT_FOUND,
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(
      password,
      restaurant.password
    );
    if (!passwordCorrecto) {
      return {
        success: false,
        message: STATUS_MESSAGES.INCORRECT_PASSWORD,
        data: null,
      };
    }
    return {
      success: true,
      message: "Bienvenido a My Store by Wilbby",
      data: {
        token: crearToken(restaurant, process.env.SECRETO, "9999 years"),
        id: restaurant._id,
      },
    };
  },

  autenticarUsuario: async (root: any, { email, password, input }) => {
    const users = await userSchema.findOne({ email });
    if (!users) {
      return {
        success: false,
        message: "Aún no de te has registrado en Wilbby",
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(password, users.password);
    if (!passwordCorrecto) {
      return {
        success: false,
        message: "Contraseña incorrecta",
        data: null,
      };
    } else {
      LoginEmail(email, input, users);
      saveDataLogin(users._id, input);
      return {
        success: true,
        message: "Bienvenido a Wilbby",
        data: {
          token: crearToken(users, process.env.SECRETO, "9999 years"),
          id: users._id,
          user: users,
        },
      };
    }
  },

  autenticarRiders: async (root: any, { email, password }) => {
    const users = await ridersSchema.findOne({ email });
    if (!users) {
      return {
        success: false,
        message: STATUS_MESSAGES.USER_NOT_FOUND,
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(password, users.password);
    if (!passwordCorrecto) {
      return {
        success: false,
        message: STATUS_MESSAGES.INCORRECT_PASSWORD,
        data: null,
      };
    }
    return {
      success: true,
      message: "Bienvenid@ a Wilbby®",
      data: {
        token: crearToken(users, process.env.SECRETO, "9999 years"),
        id: users._id,
      },
    };
  },

  crearUsuario: async (root: any, { input }) => {
    SaveEmail(input.email, input.nombre);
    SaveEmailWilbby(input.email, input.nombre, input.apellidos);
    welcomeEmail(input.email, input.nombre);
    // check if email exists
    const emailExists = await userSchema.findOne({ email: input.email });
    if (emailExists) {
      return {
        success: false,
        message: "Ya eres parte de Wilbby",
        data: null,
      };
    }

    const nuevoUsuario = new userSchema({
      name: input.nombre,
      lastName: input.apellidos,
      email: input.email,
      password: input.password,
      city: input.city,
      termAndConditions: input.termAndConditions,
    });

    nuevoUsuario.id = nuevoUsuario._id;

    return new Promise((resolve, object) => {
      nuevoUsuario.save(async (error) => {
        if (error)
          object({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else {
          resolve({
            success: true,
            message: "Usuario agregado con éxito",
            data: nuevoUsuario,
          });

          /*  CratePaypalUser(
            input.nombre,
            input.apellidos,
            input.emai,
            nuevoUsuario._id
          ); */

          await stripe.customers.create(
            {
              name: input.nombre,
              email: input.email,
              description: "Clientes de Wilbby",
            },
            function (err: any, customer: any) {
              userSchema.findOneAndUpdate(
                { _id: nuevoUsuario._id },
                {
                  $set: {
                    StripeID: customer.id,
                  },
                },
                (err, customers) => {
                  if (err) {
                  }
                }
              );
            }
          );
        }
      });
    });
  },

  crearRiders: async (root: any, { input }) => {
    // check if email exists
    const emailExists = await ridersSchema.findOne({ email: input.email });
    if (emailExists) {
      return {
        success: false,
        message: "Ya eres parte de Wilbby",
        data: null,
      };
    }

    const nuevoUsuario = new ridersSchema({
      name: input.nombre,
      lastName: input.apellidos,
      email: input.email,
      password: input.password,
      termAndConditions: input.termAndConditions,
      telefono: input.telefono,
      isAvalible: input.isAvalible,
      city: input.city,
      contactCode: input.contactCode,
    });

    nuevoUsuario.id = nuevoUsuario._id;

    return new Promise((resolve, object) => {
      nuevoUsuario.save((error) => {
        if (error)
          object({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else {
          resolve({
            success: true,
            message: "Usuario agregado con éxito",
            data: nuevoUsuario,
          });
        }
      });
    });
  },

  crearAdmin: async (root: any, { input }) => {
    // check if email exists
    const emailExists = await userAdminSchema.findOne({ email: input.email });
    if (emailExists) {
      return {
        success: false,
        message: "Ya eres parte de Wilbby",
        data: null,
      };
    }

    const nuevoUsuario = new userAdminSchema({
      name: input.nombre,
      lastName: input.apellidos,
      email: input.email,
      password: input.password,
      termAndConditions: input.termAndConditions,
    });

    nuevoUsuario.id = nuevoUsuario._id;

    return new Promise((resolve, object) => {
      nuevoUsuario.save((error) => {
        if (error)
          object({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else {
          resolve({
            success: true,
            message: "Usuario agregado con éxito",
            data: nuevoUsuario,
          });
        }
      });
    });
  },

  autenticarAdmin: async (root: any, { email, password }) => {
    const users = await userAdminSchema.findOne({ email });
    if (!users) {
      return {
        success: false,
        message: STATUS_MESSAGES.USER_NOT_FOUND,
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(password, users.password);
    if (!passwordCorrecto) {
      return {
        success: false,
        message: STATUS_MESSAGES.INCORRECT_PASSWORD,
        data: null,
      };
    }
    return {
      success: true,
      message: "Bienvenid@ a Wilbby®",
      data: {
        token: crearToken(users, process.env.SECRETO, "9999 years"),
        id: users._id,
      },
    };
  },

  singleUpload(parent: any, { file }) {
    const matches = file.match(/^data:.+\/(.+);base64,(.*)$/);
    const ext = matches[1];
    const base64_data = matches[2];
    const buffer = Buffer.from(base64_data, "base64");

    const filename = `${Date.now()}-file.${ext}`;
    const filenameWithPath = `${__dirname}/../../uploads/images/${filename}`;

    return new Promise((resolve, reject) => {
      fs.writeFile(filenameWithPath, buffer, (error) => {
        if (error) {
          reject(error);
        } else {
          resolve({ filename });
        }
      });
    });
  },

  singleUploadToAws(parent: any, { file }) {
    const buf = Buffer.from(
      file.replace(/^data:image\/\w+;base64,/, ""),
      "base64"
    );
    var data = {
      Key: `${Date.now()}-product-wilbby`,
      Body: buf,
      ContentEncoding: "base64",
      ContentType: "image/jpeg",
      ACL: "public-read",
    };
    return new Promise((resolve, reject) => {
      //@ts-ignore
      s3Bucket.upload(data, function (err, data) {
        if (err) {
          reject({
            data: null,
          });
        } else {
          resolve({
            data: data,
          });
        }
      });
    });
  },

  avatarUploadToAws(parent: any, { file, id }) {
    const buf = Buffer.from(
      file.replace(/^data:image\/\w+;base64,/, ""),
      "base64"
    );
    var data = {
      Key: `${Date.now()}-avatar-wilbby`,
      Body: buf,
      ContentEncoding: "base64",
      ContentType: "image/jpeg",
      ACL: "public-read",
    };
    return new Promise((resolve, reject) => {
      //@ts-ignore
      s3Bucket.upload(data, function (err, data) {
        if (err) {
          reject({
            data: null,
          });
        } else {
          userSchema.findOneAndUpdate(
            { _id: id },
            { avatar: data.Location },
            { new: true },
            (error, _usuario) => {
              if (error) reject(error);
              else {
                resolve({
                  data: { data, _usuario },
                });
              }
            }
          );
        }
      });
    });
  },

  createCategory: (root: any, { input }) => {
    const nuevaCategoria = new categoriaSchema({
      title: input.title,
      image: input.image,
      description: input.description,
      sorting: input.sorting,
      excludeCity: input.excludeCity,
      visible: input.visible,
      navigate: input.navigate,
      url: input.url,
    });
    return new Promise((resolve, rejects) => {
      nuevaCategoria.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.USER_ADDED,
            success: true,
          });
        }
      });
    });
  },

  createTipo: (root: any, { input }) => {
    const nuevatipo = new tipoSchema({
      title: input.title,
      image: input.image,
      description: input.description,
    });
    return new Promise((resolve, rejects) => {
      nuevatipo.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.USER_ADDED,
            success: true,
          });
        }
      });
    });
  },

  createHighkitchenCategory: (root: any, { input }) => {
    const highkitchenCategory = new highkitchenCategorySchema({
      title: input.title,
      image: input.image,
      description: input.description,
    });
    return new Promise((resolve, rejects) => {
      highkitchenCategory.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.USER_ADDED,
            success: true,
          });
        }
      });
    });
  },

  createTipoTienda: (root: any, { input }) => {
    const nuevatipo = new tipotiendaSchema({
      title: input.title,
      image: input.image,
      description: input.description,
    });
    return new Promise((resolve, rejects) => {
      nuevatipo.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.USER_ADDED,
            success: true,
          });
        }
      });
    });
  },

  eliminarTipoTienda: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      tipotiendaSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error) rejects(error);
        else resolve("Eliminado correctamente");
      });
    });
  },

  eliminarTipo: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      tipoSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error) rejects(error);
        else resolve("Eliminado correctamente");
      });
    });
  },

  eliminarCategory: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      categoriaSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error) rejects(error);
        else resolve("Eliminado correctamente");
      });
    });
  },

  actualizarAdmin: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      userAdminSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error, _usuario) => {
          if (error) reject(error);
          else resolve(_usuario);
        }
      );
    });
  },

  actualizarUsuario: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      userSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error, _usuario) => {
          if (error) reject(error);
          else resolve(_usuario);
        }
      );
    });
  },

  actualizarRiders: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      ridersSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error, _usuario) => {
          if (error) reject(error);
          else resolve(_usuario);
        }
      );
    });
  },

  eliminarAdmin: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      userAdminSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else {
          resolve({
            messages: "Usuario eliminado con éxito",
            success: true,
          });
        }
      });
    });
  },

  eliminarUsuario: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      userSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else {
          resolve({
            messages: "Usuario eliminado con éxito",
            success: true,
          });
        }
      });
    });
  },

  eliminarRiders: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      ridersSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Usuario eliminado con éxito",
            success: true,
          });
      });
    });
  },

  createRestaurant: (root: any, { input }) => {
    const nuevoRestaurant = new restaurantSchema({
      title: input.title,
      image: input.image,
      description: input.description,
      categoryName: input.categoryName,
      categoryID: input.categoryID,
      minime: input.minime,
      shipping: input.shipping,
      extras: input.extras,
      phone: input.phone,
      autoshipping: input.autoshipping,
      alegeno_url: input.alegeno_url,
      email: input.email,
      logo: input.logo,
      inOffert: input.inOffert,
      previous_shipping: input.previous_shipping,
      tipo: input.tipo,
      password: input.password,
      includeCity: input.includeCity,
      type: input.type,
      open: input.open,
      isnew: input.isnew,
      llevar: input.llevar,
      highkitchen: input.highkitchen,
      stimateTime: input.stimateTime,
      slug: input.slug,
      ispartners: input.ispartners,
      schedule: input.schedule,
      channelLinkId: input.channelLinkId,
      collections: input.collections,
      isDeliverectPartner: input.isDeliverectPartner,
      adress: input.adress,
      city: input.city,
      contactCode: input.contactCode,
      socialLink: input.socialLink,
      salvingPack: input.salvingPack,
      scheduleOnly: input.scheduleOnly,
      noScheduled: input.noScheduled,
      shorting: input.shorting,
    });
    return new Promise((resolve, rejects) => {
      nuevoRestaurant.save((error: any, res: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  eliminarRestaurant: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      restaurantSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Restaurante eliminado con éxito",
            success: true,
          });
      });
    });
  },

  actualizarRestaurant: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      restaurantSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error) => {
          if (error)
            reject({
              messages:
                "Hubo un error con tu solicitud vuelve a intentalo por favor",
              success: false,
            });
          else
            resolve({
              messages: "Datos actualizado con éxito",
              success: true,
            });
        }
      );
    });
  },

  crearFavorito: async (
    root: any,
    { restaurantID, usuarioId },
    { usuarioActual }
  ) => {
    const favorito = new favoritoSchema({
      usuarioId,
      restaurantID,
    });
    return new Promise((resolve, reject) => {
      favorito.save((error: any) => {
        if (error) {
          reject({
            messages: "Hay un problema con tu solicitud",
            success: false,
          });
        } else {
          resolve({
            messages: "Farmacia añadido a favorito",
            success: true,
          });
        }
      });
    });
  },

  eliminarFavorito: async (root: any, { id, userID }) => {
    return new Promise((resolve, reject) => {
      favoritoSchema.findOneAndDelete(
        { restaurantID: id, usuarioId: userID },
        (error, data) => {
          console.log(error);
          if (error)
            reject({
              messages: "Hay un problema con tu solicitud",
              success: false,
            });
          else
            resolve({
              messages: "Restaurante eliminado de favorito",
              success: true,
            });
        }
      );
    });
  },

  createProduct: (root: any, { input }) => {
    input.data.price = Math.round(input.data.price);

    const newProduct = new productSchema(input.data);
    return new Promise((resolve, rejects) => {
      newProduct.save((error: any, product: any) => {
        console.log(newProduct._id, newProduct.name);
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  createBundled: (root: any, { input }) => {
    const newBundled = new bundledSchema(input.data);
    return new Promise((resolve, rejects) => {
      newBundled.save((error: any, bundled) => {
        console.log(bundled);
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  createModifieldGroup: (root: any, { input }) => {
    const newmodifieldGroup = new modifieldGroup(input.data);
    return new Promise((resolve, rejects) => {
      newmodifieldGroup.save((error: any, bundled) => {
        console.log(bundled);
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  createModifield: (root: any, { input }) => {
    input.data.price = Math.round(input.data.price);
    const newmodifield = new modifielSchema(input.data);
    return new Promise((resolve, rejects) => {
      newmodifield.save((error: any, bundled) => {
        console.error(bundled._id, bundled.name);
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  crearCupon: async (root: any, { input }) => {
    try {
      const nuevoCupon = new Cupones({
        clave: input.clave,
        descuento: input.descuento,
        tipo: input.tipo,
        usage: input.usage,
        expire: input.expire,
        exprirable: input.exprirable,
        user: input.user,
        description: input.description,
        private: input.private,
        uniqueStore: input.uniqueStore,
        store: input.store,
        city: input.city,
      });
      return new Promise((resolve, reject) => {
        nuevoCupon.save(async (error, cupon) => {
          if (error) {
            return reject(error);
          } else {
            if (input.private) {
              const user = await userSchema.findOne({ _id: input.user });
              sendNotification(user?.OnesignalID, input.description);
            }
            return resolve(cupon);
          }
        });
      });
    } catch (error) {
      return new Promise((_resolve, reject) => {
        return reject({
          success: false,
          message: "Hay un problema con su solicitud",
          data: null,
        });
      });
    }
  },

  eliminarCupon: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      Cupones.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            message: "Miembro eliminado con éxito",
          });
      });
    });
  },

  crearValoracion: async (root: any, { input }) => {
    try {
      const nuevoValoracion = new RatingSchema({
        user: input.user,
        comment: input.comment,
        value: input.value,
        restaurant: input.restaurant,
      });

      return new Promise((resolve, reject) => {
        nuevoValoracion.save((error, rating) => {
          if (error) {
            return reject(error);
          } else {
            updateRestaurant(input.restaurant);
            return resolve(rating);
          }
        });
      });
    } catch (error) {
      return new Promise((_resolve, reject) => {
        return reject({
          success: false,
          message: "Hay un problema con su solicitud",
          data: null,
        });
      });
    }
  },

  createOpinion: (root: any, { input }) => {
    const nuevaOpinion = new OpinionSchema({
      plato: input.plato,
      comment: input.comment,
      rating: input.rating,
      user: input.user,
    });
    return new Promise((resolve, rejects) => {
      nuevaOpinion.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  crearPago: (root: any, { input }) => {
    const nuevoPago = new pagoSchema({
      nombre: input.nombre,
      iban: input.iban,
      restaurantID: input.restaurantID,
    });

    nuevoPago.id = nuevoPago._id;

    return new Promise((resolve, reject) => {
      nuevoPago.save((error) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Pago añadido con éxito",
            success: true,
          });
      });
    });
  },

  eliminarPago: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      pagoSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Pago eliminado con éxito",
            success: true,
          });
      });
    });
  },

  crearDeposito: (root: any, { input }) => {
    const nuevoDeposito = new transSchema({
      fecha: new Date(),
      estado: input.estado,
      total: input.total,
      restaurantID: input.restaurantID,
    });
    nuevoDeposito.id = nuevoDeposito._id;
    return new Promise((resolve, reject) => {
      nuevoDeposito.save((error: any) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Deposito añadido con éxito",
            success: true,
          });
      });
    });
  },

  eliminarDeposito: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      transSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            success: false,
            messages: "Hay un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            messages: "Deposito eliminado con éxito",
          });
      });
    });
  },

  createAdress: (root: any, { input }) => {
    const nuevaadress = new adressSchema({
      formatted_address: input.formatted_address,
      puertaPiso: input.puertaPiso,
      type: input.type,
      usuario: input.usuario,
      city: input.city,
      postalcode: input.postalcode,
      lat: input.lat === "null" ? "39.1582846" : input.lat,
      lgn: input.lgn === "null" ? "-3.0215836" : input.lgn,
    });
    return new Promise((resolve, rejects) => {
      nuevaadress.save((error: any, adress: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: adress,
          });
        }
      });
    });
  },

  createStoreAdress: (root: any, { input }) => {
    const nuevaadress = new adressStoreSchema({
      calle: input.calle,
      numero: input.numero,
      codigoPostal: input.codigoPostal,
      ciudad: input.ciudad,
      store: input.store,
      lat: input.lat,
      lgn: input.lgn,
    });
    return new Promise((resolve, rejects) => {
      nuevaadress.save((error: any, adress: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            success: true,
            message: STATUS_MESSAGES.DATA_SUCCESS,
            data: adress,
          });
        }
      });
    });
  },

  actualizarAdress: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      adressSchema.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error, adress) => {
          if (error) reject(error);
          else
            resolve({
              messages: "Dirección actualizada con éxito",
              success: true,
              data: adress,
            });
        }
      );
    });
  },

  eliminarAdress: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      adressSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Dirección eliminado con éxito",
            success: true,
          });
      });
    });
  },

  eliminarAdressStore: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      adressStoreSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Dirección eliminado con éxito",
            success: true,
          });
      });
    });
  },

  eliminarPost: async (root: any, { id }, { usuarioActual }) => {
    const isAdmin = await userAdminSchema.findOne({ _id: usuarioActual._id });
    if (!isAdmin) {
      return {
        success: true,
        messages: "No estas autorizado para eliminar un post",
        data: null,
      };
    } else {
      return new Promise((resolve, reject) => {
        postSchema.findOneAndDelete({ _id: id }, (error) => {
          if (error)
            reject({
              message: "Hay un problema con su solicitud",
              success: false,
            });
          else
            resolve({
              messages: "Dirección eliminado con éxito",
              success: true,
            });
        });
      });
    }
  },

  actualizarPost: async (root: any, { input }, { usuarioActual }) => {
    const isAdmin = await userAdminSchema.findOne({ _id: usuarioActual._id });
    if (!isAdmin) {
      return {
        success: true,
        messages: "No estas autorizado para actualizar un post",
        data: null,
      };
    } else {
      return new Promise((resolve, reject) => {
        postSchema.findOneAndUpdate(
          { _id: input.id },
          input,
          { new: true },
          (error, post) => {
            if (error) reject(error);
            else
              resolve({
                success: true,
                message: "Dirección actualizada con éxito",
                data: post,
              });
          }
        );
      });
    }
  },

  createPost: async (root: any, { input }, { usuarioActual }) => {
    const isAdmin = await userAdminSchema.findOne({ _id: usuarioActual._id });
    if (!isAdmin) {
      return {
        success: true,
        message: "No estas autorizado para crear un post",
        data: null,
      };
    } else {
      const nuevoPost = new postSchema({
        title: input.title,
        image: input.image,
        shortDescription: input.shortDescription,
        like: input.like,
        tags: input.tags,
        author: input.author,
        category: input.category,
        readTime: input.readTime,
        content: input.content,
        slug: input.slug,
        country: input.country,
      });
      return new Promise((resolve, rejects) => {
        nuevoPost.save((error: any, post: any) => {
          if (error) {
            rejects(error);
          } else {
            resolve({
              success: true,
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              data: post,
            });
          }
        });
      });
    }
  },

  createCustonOrder: async (root: any, { input }, { usuarioActual }) => {
    const isAdmin = await userSchema.findOne({ _id: usuarioActual._id });
    if (!isAdmin) {
      return {
        success: true,
        messages: "No estas autorizado para crear un esta orden",
        data: null,
      };
    } else {
      const nuevaOrder = new customOrderSchema({
        display_id: input.display_id,
        riders: input.riders,
        origin: input.origin,
        destination: input.destination,
        schedule: input.schedule,
        distance: input.distance,
        nota: input.nota,
        date: input.date,
        city: input.city,
        userID: input.userID,
        estado: input.estado,
        status: input.status,
        progreso: input.progreso,
        total: input.total,
        product_stimate_price: input.product_stimate_price,
      });
      return new Promise((resolve, rejects) => {
        nuevaOrder.save((error: any, order: any) => {
          if (error) {
            rejects(error);
          } else {
            resolve({
              success: true,
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              data: order,
            });
          }
        });
      });
    }
  },

  actualizarOrderProcess: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      customOrderSchema.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error, order: any) => {
          if (error) {
            reject(error);
          } else {
            SendNotificationCustomOrder(order);
            resolve({
              success: true,
              messages: "Orden actualizada",
              data: order,
            });
          }
        }
      );
    });
  },

  actualizarCity: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      citySchema.findOneAndUpdate(
        { _id: input.id },
        input,
        { new: true },
        (error) => {
          if (error)
            reject({
              messages: "Algo va mal intentalo de Nuevo",
              success: true,
            });
          else
            resolve({
              messages: "Dirección actualizada con éxito",
              success: true,
            });
        }
      );
    });
  },

  createCity: async (root: any, { input }) => {
    const nuevaCity = new citySchema({
      close: input.close,
      city: input.city,
      title: input.title,
      subtitle: input.subtitle,
      imagen: input.imagen,
    });
    return new Promise((resolve, rejects) => {
      nuevaCity.save((error: any, order: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  createOffert: async (root: any, { input }) => {
    const nuevaOffert = new offertsSchema({
      store: input.store,
      slug: input.slug,
      city: input.city,
      apertura: input.apertura,
      cierre: input.cierre,
      imagen: input.imagen,
      open: input.open,
    });
    return new Promise((resolve, rejects) => {
      nuevaOffert.save((error: any, order: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  eliminarOfferts: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      offertsSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            messages: "Algo va mal intentalo de Nuevo",
            success: true,
          });
        else
          resolve({
            messages: "Oferta eliminada con éxito",
            success: true,
          });
      });
    });
  },

  createTransactionRider: async (root: any, { input }) => {
    const nuevatransaction = new transactionSchema({
      rider: input.rider,
      km: input.km,
      order: input.order,
      total: input.total,
      propina: input.propina,
      iva: input.iva,
    });
    return new Promise((resolve, rejects) => {
      nuevatransaction.save(async (error: any, order: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          const riders = await ridersSchema.findOne({ _id: input.rider });
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  createQuincena: async (root: any, { input }) => {
    const nuevaquincena = new quincenaSchama({
      fromDate: input.fromDate,
      toDate: input.toDate,
      numberQuincena: input.numberQuincena,
    });
    return new Promise((resolve, rejects) => {
      nuevaquincena.save((error: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  createCollection: async (root: any, { input }) => {
    const nuevaCollection = new collectionSchema({
      title: input.title,
      image: input.image,
      store: input.store,
      sorting: input.sorting,
    });
    return new Promise((resolve, rejects) => {
      nuevaCollection.save((error: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  createsubCollection: async (root: any, { input }) => {
    const nuevasubCollection = new subCollectionSchema({
      title: input.title,
      collectiontype: input.collectiontype,
      sorting: input.sorting,
    });
    return new Promise((resolve, rejects) => {
      nuevasubCollection.save((error: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  addToCart: async (root: any, { input }) => {
    const newCart = new newCartSchema({
      userId: input.userId,
      storeId: input.storeId,
      productId: input.productId,
      items: input.items,
      addToCart: input.addToCart,
    });

    const exist = await newCartSchema.findOne({ productId: input.productId });

    if (exist) {
      return new Promise((resolve, rejects) => {
        newCartSchema
          .deleteMany({ productId: input.productId })
          .then(function () {
            newCart.save((error: any) => {
              if (error) {
                rejects({
                  messages: "Algo va mal intentalo de nuevo",
                  success: true,
                });
              } else {
                resolve({
                  messages: "Producto añadido a la cesta",
                  success: true,
                });
              }
            });
          })
          .catch(function (error) {
            console.log(error); // Failure
          });
        /*  newCartSchema.findOneAndUpdate(
          { productId: input.productId },
          input,
          { new: true },
          (error: any) => {
            if (error) {
              rejects({
                messages: "Algo va mal intentalo de nuevo",
                success: true,
              });
            } else {
              resolve({
                messages: "Producto añadido a la cesta",
                success: true,
              });
            }
          }
        ); */
      });
    } else {
      return new Promise((resolve, rejects) => {
        newCart.save((error: any) => {
          if (error) {
            rejects({
              messages: "Algo va mal intentalo de nuevo",
              success: true,
            });
          } else {
            resolve({
              messages: "Producto añadido a la cesta",
              success: true,
            });
          }
        });
      });
    }
  },

  deleteCartItem: async (root: any, { id, fromWeb }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para comtinuar",
        success: false,
      };
    }

    let condition = {};
    if (fromWeb) {
      condition = { productId: id };
    } else {
      condition = { _id: id };
    }
    return new Promise((resolve, reject) => {
      newCartSchema.findOneAndDelete(condition, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con tu solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Producto eliminado de la cesta",
            success: true,
          });
      });
    });
  },

  crearModificarNewOrden: async (root: any, { input }, { usuarioActual }) => {
    try {
      if (!usuarioActual) {
        return new Promise((_resolve, reject) => {
          return _resolve({
            success: false,
            message: "Debes iniciar sesión para continuar",
            data: null,
          });
        });
      }

      let courrier: any;
      courrier = await ridersSchema.findOne({
        _id: input.courier ? input.courier : null,
      });

      let orden: any;
      if (input._id) {
        orden = await newOrderSchema.findById(ObjectId(input._id)).exec();
        orden.set({
          courier: input.courier ? input.courier : null,
          courierData: courrier,
        });
      }

      if (!input.store) {
        const restaurants = await restaurantSchema
          .findById(ObjectId(input.store))
          .exec();
        if (!restaurants) {
          return new Promise((_resolve, reject) => {
            return _resolve({
              success: false,
              message: "Restaurante no disponible",
              data: null,
            });
          });
        }
      }

      let cupon: any;
      if (!!input.clave) {
        const cup = await Cupones.findOne({ clave: input.clave }).exec();
        const existOrder = await newOrderSchema.find({
          customer: input.customer,
          cupon: cup?._id,
          status: { $ne: "Pendiente de pago" },
        });

        if (!cup) {
          return new Promise((_resolve, reject) => {
            return _resolve({
              success: false,
              message: "Este cupón no existe",
              data: null,
            });
          });
        } else if (existOrder.length >= cup.usage) {
          return new Promise((_resolve, reject) => {
            return _resolve({
              success: false,
              message: "Ya haz utilizado este cupón prueba con otro código",
              data: null,
            });
          });
        } else if (cup.exprirable) {
          if (cup.expire < new Date()) {
            return new Promise((_resolve, reject) => {
              return _resolve({
                success: false,
                message: "Este cupòn ya ha expirado",
                data: null,
              });
            });
          } else {
            cupon = cup;
          }
        } else {
          cupon = cup;
        }
      } else if (!!input.cupon) {
        cupon = input.cupon;
      }

      let userData: any;
      userData = await userSchema.findOne({
        _id: input.customer,
      });

      let AdreesData: any;
      if (input.deliveryAddress) {
        AdreesData = await adressSchema.findOne({
          _id: input.deliveryAddress,
        });
      } else {
        AdreesData = await adressSchema.findOne({
          usuario: input.customer,
        });
      }

      if (input.orderType === "delivery" && !AdreesData) {
        return new Promise((_resolve, reject) => {
          return _resolve({
            success: false,
            message: "Debes añadir una dirección para continuar",
            data: null,
          });
        });
      }

      let StoreData: any;
      StoreData = await restaurantSchema.findOne({
        _id: input.store,
      });

      let product: any;
      product = await newCartSchema.find({
        userId: input.customer,
        storeId: input.store,
      });

      if (!product) {
        return new Promise((_resolve, reject) => {
          return _resolve({
            success: false,
            message: "No hay productos para este pedido",
            data: null,
          });
        });
      }

      let discuntTotal = 0;

      if (cupon) {
        const deTo = (input.payment * cupon.descuento) / 100;
        switch (cupon.tipo) {
          case "dinero":
            discuntTotal = cupon.descuento;
            break;
          case "porcentaje":
            discuntTotal = deTo;
            break;
        }
      }

      if (!orden || !orden._id) {
        orden = new newOrderSchema({
          cupon: cupon && cupon._id ? cupon._id : cupon,
          channelOrderDisplayId: input.channelOrderDisplayId,
          orderType: input.orderType,
          pickupTime: input.pickupTime,
          estimatedPickupTime: input.estimatedPickupTime,
          deliveryTime: input.deliveryTime,
          courier: input.courier,
          courierData: courrier,
          statusProcess: [],
          customerData: userData,
          customer: input.customer,
          store: input.store,
          storeData: StoreData,
          deliveryAddressData: AdreesData,
          deliveryAddress: input.deliveryAddress,
          payment: input.payment,
          note: input.note,
          items: input.items ? input.items : product,
          tip: input.tip,
          numberOfCustomers: input.numberOfCustomers,
          deliveryCost: input.deliveryCost,
          serviceCharge: input.serviceCharge,
          discountTotal: Math.round(discuntTotal),
          IntegerValue: input.IntegerValue,
          Needcutlery: input.Needcutlery,
          scheduled: input.scheduled,
        });
      } else {
        if (!orden.cupon) {
          orden.set({ cupon: cupon ? cupon._id : null });
        }
      }

      return new Promise((resolve, reject) => {
        orden.save((error: any, ordenguardada: any) => {
          if (error) {
            return resolve({
              success: false,
              message: "Algo salio mal intentalo de nuevo",
              data: null,
            });
          } else {
            if (!ordenguardada.descuento && cupon && cupon._id) {
              ordenguardada.descuento = cupon;
            }
            setDistanceToOrder(ordenguardada._id);
            if (input.courier && StoreData.isOtterPartner) {
              UpdateOrderRider(ordenguardada);
            }
            return resolve({
              success: true,
              message: "Todo ha salido bien",
              data: ordenguardada,
            });
          }
        });
      });
    } catch (error) {
      return new Promise((_resolve, reject) => {
        return reject({
          success: false,
          message: "Algo salio mal intentalo de nuevo",
          data: null,
        });
      });
    }
  },

  NewOrdenProceed: async (
    root: any,
    { ordenId, status, IntegerValue, statusProcess }
  ) => {
    return new Promise((resolve, reject) => {
      newOrderSchema.findOneAndUpdate(
        { _id: ordenId },
        {
          status: status,
          IntegerValue: Number(IntegerValue),
          $push: { statusProcess: statusProcess },
        },
        { upsert: true },
        //@ts-ignore
        async (error: any, order: IOrders) => {
          if (error) {
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
            });
          } else {
            pushNotificationOrderProccess(status, order);
            if (status === "Devuelto") {
              deleteRecipt(order);
            }
            resolve({
              messages: "Orden procesada con éxito",
              success: true,
            });
          }
        }
      );
    });
  },

  snoozedProduct: async (root: any, { id, snoozed }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para continiar",
        success: false,
      };
    }
    return new Promise((resolve, reject) => {
      productSchema.findOneAndUpdate(
        { _id: id },
        { snoozed: snoozed },
        { upsert: true },
        (error) => {
          if (error)
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
            });
          else
            resolve({
              messages: "Producto marcado como agotado",
              success: true,
            });
        }
      );
    });
  },

  snoozedBundled: async (root: any, { id, snoozed }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para continiar",
        success: false,
      };
    }
    return new Promise((resolve, reject) => {
      bundledSchema.findOneAndUpdate(
        { _id: id },
        { snoozed: snoozed },
        { upsert: true },
        (error) => {
          if (error)
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
            });
          else
            resolve({
              messages: "Modificador marcado como agotado",
              success: true,
            });
        }
      );
    });
  },
  snoozedModifield: async (root: any, { id, snoozed }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para continiar",
        success: false,
      };
    }
    return new Promise((resolve, reject) => {
      modifielSchema.findOneAndUpdate(
        { _id: id },
        { snoozed: snoozed },
        { upsert: true },
        (error, data) => {
          console.log(data);
          if (error)
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
            });
          else
            resolve({
              messages: "Modificador marcado como agotado",
              success: true,
            });
        }
      );
    });
  },

  snoozedModifieldGroup: async (
    root: any,
    { id, snoozed },
    { usuarioActual }
  ) => {
    console.log(id, snoozed);
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para continiar",
        success: false,
      };
    }
    return new Promise((resolve, reject) => {
      modifieldGroup.findOneAndUpdate(
        { _id: id },
        { snoozed: snoozed },
        { upsert: true },
        (error) => {
          if (error)
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
            });
          else
            resolve({
              messages: "Modificador marcado como agotado",
              success: true,
            });
        }
      );
    });
  },

  createMenu: async (root: any, { input }) => {
    const nuevaMenu = new menuSchema(input.data);
    return new Promise((resolve, rejects) => {
      nuevaMenu.save((error: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  createNewCategory: async (root: any, { input }) => {
    const nuevaCategory = new categorySchema(input.data);
    return new Promise((resolve, rejects) => {
      nuevaCategory.save((error: any) => {
        if (error) {
          rejects({
            messages: "Algo va mal intentalo de nuevo",
            success: true,
          });
        } else {
          resolve({
            messages: "Datos guardado con éxito",
            success: true,
          });
        }
      });
    });
  },

  RiderAccepOrder: async (root: any, { id, riderAcceptOrder }) => {
    return new Promise((resolve, reject) => {
      newOrderSchema.findOneAndUpdate(
        { _id: id },
        {
          riderAcceptOrder: riderAcceptOrder,
        },
        { upsert: true },
        //@ts-ignore
        async (error: any, order: IOrders) => {
          if (error) {
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
            });
          } else {
            setToHolded(
              order.courierData,
              order.reportRiders.total,
              order.channelOrderDisplayId
            );
            resolve({
              messages: "Orden procesada con éxito",
              success: true,
            });
          }
        }
      );
    });
  },

  SendOffertOrder: async (root: any, { id, input }) => {
    return new Promise((resolve, reject) => {
      newOrderSchema.findOneAndUpdate(
        { _id: id },
        {
          $push: { offertFromRider: input },
        },
        { upsert: true },
        //@ts-ignore
        async (error: any, order: IOrders) => {
          if (error) {
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
            });
          } else {
            resolve({
              messages: "Oferta enviada con éxito",
              success: true,
            });
          }
        }
      );
    });
  },

  eliminarProducto: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      productSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else {
          resolve({
            messages: "Producto eliminado con éxito",
            success: true,
          });
        }
      });
    });
  },

  actualizarProduct: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      productSchema.findOneAndUpdate(
        { _id: input.data._id },
        input.data,
        { new: true },
        (error) => {
          if (error) reject(error);
          else
            resolve({
              messages: "Producto Actualizado",
              success: true,
            });
        }
      );
    });
  },

  actualizarCategoria: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      categorySchema.findOneAndUpdate(
        { _id: input.data._id },
        input.data,
        { new: true },
        (error, datos: ICategoryProduct | null) => {
          if (error) {
            reject(error);
          } else {
            if (input.data.snoozed && datos) {
              datos &&
                datos.products.forEach(async (x) => {
                  const prod = await productSchema.findOne({ _id: x });
                  productSchema.findOneAndUpdate(
                    { _id: prod?._id },
                    {
                      snoozed: true,
                    },
                    { upsert: true },
                    () => {
                      console.log("done");
                    }
                  );
                });
            } else {
              datos &&
                datos.products.forEach(async (x) => {
                  const prod = await productSchema.findOne({ _id: x });
                  productSchema.findOneAndUpdate(
                    { _id: prod?._id },
                    {
                      snoozed: false,
                    },
                    { upsert: true },
                    () => {
                      console.log("done");
                    }
                  );
                });
            }
            resolve({
              messages: "Producto Actualizado",
              success: true,
            });
          }
        }
      );
    });
  },

  eliminarCategoria: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      categorySchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else {
          resolve({
            messages: "Producto eliminado con éxito",
            success: true,
          });
        }
      });
    });
  },

  createCardSource: (root: any, { customers, paymentMethod }) => {
    return new Promise(async (resolve, reject) => {
      stripe.paymentMethods
        .attach(paymentMethod, {
          customer: customers,
        })
        .then((res) => {
          resolve({
            messages: "Tarjeta añadida al wallet",
            success: true,
          });
        })
        .catch((e) => {
          reject({
            messages: "Algo salio mal intentalo de nuevo",
            success: false,
          });
        });
    });
  },

  createAds: (root: any, { data }) => {
    const nuevaAds = new adsSchema(data.data);
    return new Promise((resolve, rejects) => {
      nuevaAds.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.USER_ADDED,
            success: true,
          });
        }
      });
    });
  },

  eliminarAds: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      adsSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else {
          resolve({
            messages: "Producto eliminado con éxito",
            success: true,
          });
        }
      });
    });
  },
};
