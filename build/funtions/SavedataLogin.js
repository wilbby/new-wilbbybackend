"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.saveDataLogin = void 0;
const DataLoginUser_1 = __importDefault(require("../models/DataLoginUser"));
exports.saveDataLogin = (user, data) => {
    const nevodataLogin = new DataLoginUser_1.default({
        user: user,
        data: data,
    });
    nevodataLogin.save();
};
