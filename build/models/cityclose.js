"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const citySchema = new mongoose_1.default.Schema({
    close: {
        type: Boolean,
    },
    city: {
        type: String,
    },
    title: {
        type: String,
    },
    subtitle: {
        type: String,
    },
    imagen: {
        type: String,
    },
});
exports.default = mongoose_1.default.model("cities", citySchema);
