"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ordenUpdate = void 0;
const order_1 = __importDefault(require("../models/newOrder/order"));
exports.ordenUpdate = (orderID, estado, progreso) => {
    const st = { status: estado, date: new Date() };
    order_1.default.findOneAndUpdate({ _id: orderID }, {
        status: estado,
        IntegerValue: progreso,
        $push: { statusProcess: st },
    }, { upsert: true }, (err, order) => { });
};
