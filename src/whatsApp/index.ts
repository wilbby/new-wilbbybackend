import { Request, Response, Router } from "express";
import dotenv from "dotenv";

dotenv.config({ path: "variables.env" });

const accountSid = process.env.accountSid;
const authToken = process.env.authToken;

const client = require("twilio")(accountSid, authToken);

class WhatsAppRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  sendWhatsApp(req: Request, res: Response) {
    client.messages
      .create({
        from: "whatsapp:+15102983479",
        body:
          "Hola hemos recibido tu pedido y vamos a por el para hacerte la entrega",
        to: "whatsapp:+34664028161",
      })
      .then((message) => {
        res.status(200).json(message).end();
      });
  }

  routes() {
    this.router.get("/whatsappp", this.sendWhatsApp);
    this.router.post("/whatsapp-hook", (res: Request, req: Request) => {
      console.log(req.ServerResponse);
    });
  }
}

const whatsAppRouterRouter = new WhatsAppRouter();
whatsAppRouterRouter.routes();

export default whatsAppRouterRouter.router;
