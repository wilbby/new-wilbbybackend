import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const adressStoreSchema = new mongoose.Schema({
  calle: {
    type: String,
    required: true,
  },

  numero: {
    type: String,
  },

  codigoPostal: {
    type: String,
  },

  ciudad: {
    type: String,
  },

  store: {
    type: String,
  },

  lat: {
    type: String,
  },

  lgn: {
    type: String,
  },
});

export interface IAdress extends Document {
  calle: string;
  cp: string;
  usuario: string;
  ciudad: string;
}

export default mongoose.model<IAdress>("adressStore", adressStoreSchema);
