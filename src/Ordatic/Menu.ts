import Menu from "../models/newMenu/Menu";
import Category from "../models/newMenu/category";
import Products from "../models/newMenu/products";
import ModifieldGroup from "../models/newMenu/modifierGroups";
import Modifield from "../models/newMenu/modifiers";
import Bundled from "../models/newMenu/bundles";

export const setMenuFromOrdatic = (data, store) => {
  data.data.catalogs.forEach((element) => {
    const newMenuInput = {
      menu: element.name,
      menuId: "",
      description: element.desc,
      menuImageURL: "",
      menuType: 1,
      availabilities: [],
      snoozedProducts: [],
      productTags: [],
      currency: 1,
      validations: [],
      nestedModifiers: false,
      channelLinkId: data.storeId,
      storeId: store?._id,
    };

    const newMenu = new Menu(newMenuInput);

    Menu.deleteMany({ storeId: store?._id })
      .then(function () {
        newMenu.menuId = newMenu._id;
        newMenu.save();
      })
      .catch(function (error) {
        console.log(error); // Failure
      });

    element.sections.forEach(async (category, i) => {
      const dataCategory = {
        name: category.name,
        description: "",
        nameTranslations: {},
        descriptionTranslations: {},
        account: store?._id,
        posLocationId: store?._id,
        posCategoryType: store?._id,
        subCategories: [],
        posCategoryId: store?._id,
        imageUrl: "",
        products: category.itemsInnerIds,
        menu: newMenu._id,
        sortedChannelProductIds: [],
        subProductSortOrder: [],
        subProducts: [],
        level: 1,
        availabilities: [],
        internalId: category.innerId,
        storeId: store?._id,
        sorting: i,
      };

      const newCategory = new Category(dataCategory);

      Category.deleteMany({ account: store?._id })
        .then(function () {
          newCategory.save();
        })
        .catch(function (error) {
          console.log(error); // Failure
        });
    });

    element.items.forEach(async (product) => {
      const dataProduct = {
        name: product.name,
        description: product.desc,
        nameTranslations: {},
        descriptionTranslations: {},
        account: store?._id,
        location: store?._id,
        productType: 1,
        plu: product.posRef,
        price: product.price,
        previous_price: 0,
        priceLevels: {},
        sortOrder: 1,
        deliveryTax: 0,
        takeawayTax: 0,
        multiply: 1,
        multiMax: 1,
        posProductId: store?._id,
        posProductCategoryId: [],
        subProducts: product.modifierGroupsInnerIds,
        productTags: product.sectionsInnerIds,
        posCategoryIds: [],
        imageUrl: product.images[0] ? product.images[0].url : "",
        max: 1,
        min: 1,
        capacityUsages: [],
        parentId: "",
        visible: true,
        snoozed: false,
        subProductSortOrder: [],
        internalId: product.innerId,
        new: false,
        popular: false,
        offert: false,
        recomended: false,
        quantity: 1,
        storeId: store?._id,
      };

      Products.deleteMany({ storeId: store?._id })
        .then(async function () {
          const newProduct = new Products(dataProduct);

          const cat = await Category.find({
            //@ts-ignore
            internalId: newProduct.productTags.map((x) => {
              return x;
            }),
          });
          cat.forEach((x) => {
            newProduct.parentId = x._id;
          });

          newProduct.save();
        })
        .catch(function (error) {
          console.log(error); // Failure
        });
    });

    element.modifierGroups.forEach(async (bundled) => {
      const bundledDate = {
        name: bundled.title,
        description: "",
        nameTranslations: {},
        descriptionTranslations: {},
        account: store?._id,
        location: store?._id,
        productType: 1,
        plu: bundled.posRef,
        price: 0,
        priceLevels: {},
        sortOrder: 1,
        deliveryTax: 0,
        takeawayTax: 0,
        multiply: bundled.max,
        posProductId: "",
        posProductCategoryId: bundled.modifiersInnerIds,
        subProducts: [],
        productTags: bundled.itemsInnerIds,
        posCategoryIds: [],
        imageUrl: "",
        max: bundled.max,
        min: bundled.min,
        multiMax: bundled.max,
        capacityUsages: [],
        parentId: bundled.innerId,
        snoozed: false,
        subProductSortOrder: [],
        internalId: bundled.innerId,
        recomended: false,
        quantity: 1,
      };

      if (bundled.min > 0) {
        Bundled.deleteMany({ account: store?._id })
          .then(async function () {
            const newBundled = new Bundled(bundledDate);
            newBundled.save().then(() => {
              console.log("Save Bundled");
            });
          })
          .catch(function (error) {
            console.log(error); // Failure
          });
      } else {
        ModifieldGroup.deleteMany({ account: store?._id })
          .then(async function () {
            const newBundled = new ModifieldGroup(bundledDate);
            newBundled.save().then(() => {
              console.log("Save Modifield Group");
            });
          })
          .catch(function (error) {
            console.log(error); // Failure
          });
      }
    });

    element.modifiers.forEach(async (modifield) => {
      const dataProduct = {
        name: modifield.name,
        description: "",
        nameTranslations: {},
        descriptionTranslations: {},
        account: store?._id,
        location: store?._id,
        productType: 1,
        plu: modifield.posRef,
        price: modifield.price,
        previous_price: 0,
        priceLevels: {},
        sortOrder: 1,
        deliveryTax: 0,
        takeawayTax: 0,
        multiply: 1,
        multiMax: 1,
        posProductId: store?._id,
        posProductCategoryId: modifield.modifierGroupsInnerIds,
        subProducts: [],
        productTags: modifield.modifierGroupsInnerIds,
        posCategoryIds: [],
        imageUrl: "",
        max: 1,
        min: 1,
        capacityUsages: [],
        parentId: modifield.innerId,
        visible: true,
        snoozed: false,
        subProductSortOrder: [],
        internalId: modifield.innerId,
        new: false,
        popular: false,
        offert: false,
        recomended: false,
        quantity: 1,
        storeId: store?._id,
      };

      Modifield.deleteMany({ account: store?._id })
        .then(async function () {
          const newProduct = new Modifield(dataProduct);
          newProduct.save();
        })
        .catch(function (error) {
          console.log(error); // Failure
        });

      Products.deleteMany({ account: store?._id })
        .then(async function () {
          const newProduct = new Products(dataProduct);
          newProduct.save();
        })
        .catch(function (error) {
          console.log(error); // Failure
        });
    });
  });
};
