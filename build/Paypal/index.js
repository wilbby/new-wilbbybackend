"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const paypal_rest_sdk_1 = __importDefault(require("paypal-rest-sdk"));
const dotenv_1 = __importDefault(require("dotenv"));
const order_1 = __importDefault(require("../models/newOrder/order"));
const addToCart_1 = __importDefault(require("../models/newOrder/addToCart"));
const custonorder_1 = __importDefault(require("../models/custonorder"));
const Deliverect_1 = require("../Deliverect/Deliverect");
const api_1 = require("../Ordatic/api");
const api_2 = require("../Otter/api");
const Apis_1 = require("../Holded/Apis");
const sendNotificationToStore_1 = require("./sendNotificationToStore");
const scheduleTime_1 = require("../funtions/scheduleTime");
const ScheduledMaria_1 = require("../funtions/ScheduledMaria");
const asingRiders_1 = require("../funtions/asingRiders");
const asingRidersCustonOrder_1 = require("../funtions/asingRidersCustonOrder");
const mongoose_1 = require("mongoose");
dotenv_1.default.config({ path: "variables.env" });
const { ObjectId } = mongoose_1.Types;
paypal_rest_sdk_1.default.configure({
    mode: "live",
    client_id: process.env.PAYPALCLIENTID || "",
    client_secret: process.env.PAYPALCLIENTSECRET || "",
});
class PaypalRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/paypal", (req, res) => {
            const { price, orderid, userId, currency } = req.query;
            let create_payment_json = {
                intent: "sale",
                payer: {
                    payment_method: "paypal",
                },
                redirect_urls: {
                    return_url: `https://api.v1.wilbby.com/success?price=${price}&orderid=${orderid}&userId=${userId}&currency=${currency}`,
                    cancel_url: "https://api.v1.wilbby.com/cancel",
                },
                transactions: [
                    {
                        amount: {
                            currency: currency ? currency : "EUR",
                            total: req.query.price,
                        },
                        description: "Peinado & Perales SL (Wilbby)",
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.create(create_payment_json, function (error, payment) {
                console.log(error, payment);
                if (error) {
                    res.redirect("/cancel");
                }
                else {
                    // @ts-ignore
                    res.redirect(payment.links[1].href);
                }
            });
        });
        this.router.get("/success", (req, res) => {
            var PayerID = req.query.PayerID;
            var paymentId = req.query.paymentId;
            const { price, orderid, userId, currency } = req.query;
            var execute_payment_json = {
                payer_id: PayerID,
                transactions: [
                    {
                        amount: {
                            currency: currency ? currency : "EUR",
                            total: price,
                        },
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.execute(
            // @ts-ignore
            paymentId, execute_payment_json, function (error, payment) {
                if (error) {
                    res.redirect("/cancel");
                }
                if (payment) {
                    order_1.default.findOneAndUpdate(
                    //@ts-ignore
                    { _id: ObjectId(orderid) }, {
                        status: "Nueva",
                        IntegerValue: 30,
                        paymentMethod: "Paypal",
                        pagoPaypal: payment,
                        orderIsAlreadyPaid: true,
                        //@ts-ignore
                        $push: {
                            statusProcess: { status: "Nueva", date: new Date() },
                        },
                    }, { upsert: true }, 
                    // @ts-ignore
                    (err, order) => __awaiter(this, void 0, void 0, function* () {
                        if (!err) {
                            if (order === null || order === void 0 ? void 0 : order.storeData.isDeliverectPartner) {
                                Deliverect_1.setOrderToDeliverect(order);
                            }
                            else if (order === null || order === void 0 ? void 0 : order.storeData.isOrdaticPartner) {
                                api_1.pushOrderTopOrdatic(order);
                            }
                            else if (order === null || order === void 0 ? void 0 : order.storeData.isOtterPartner) {
                                api_2.pushOrderToOtter(order);
                            }
                            else {
                                sendNotificationToStore_1.sendNotification(order === null || order === void 0 ? void 0 : order.storeData.OnesignalID, "Has recibido un nuevo pedido a por todas 🌮 🌯 🫔 🥗 🥘");
                            }
                            if (scheduleTime_1.scheduleTime(ScheduledMaria_1.getMariaHorario().schedule) &&
                                order.orderType === "delivery") {
                                asingRiders_1.AsingRider(order);
                            }
                            Apis_1.setOrderToHolded(order);
                        }
                        addToCart_1.default
                            .deleteMany({ userId: String(userId) })
                            .then(function () {
                            console.log("Data deleted cart"); // Success
                        })
                            .catch(function (error) {
                            console.log(error); // Failure
                        });
                        res.render("success");
                    }));
                    console.log("Get Payment Response");
                }
            });
        });
        this.router.get("/cancel", (req, res) => {
            res.render("cancel");
        });
        this.router.get("/paypal-custom-order", (req, res) => {
            const { price, order, currency } = req.query;
            let create_payment_json = {
                intent: "sale",
                payer: {
                    payment_method: "paypal",
                },
                redirect_urls: {
                    return_url: `https://api.v1.wilbby.com/success-custom?price=${price}&order=${order}&currency=${currency}`,
                    cancel_url: "https://api.v1.wilbby.com/cancel-custom",
                },
                transactions: [
                    {
                        amount: {
                            currency: currency ? currency : "EUR",
                            total: req.query.price,
                        },
                        description: "Peinado & Perales SL (Wilbby)",
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.create(create_payment_json, function (error, payment) {
                if (error) {
                    res.render("cancel");
                }
                else {
                    res.render("cancel");
                    // @ts-ignore
                    res.redirect(payment.links[1].href);
                }
            });
        });
        this.router.get("/success-custom", (req, res) => {
            var PayerID = req.query.PayerID;
            var paymentId = req.query.paymentId;
            const { price, order, currency } = req.query;
            var execute_payment_json = {
                payer_id: PayerID,
                transactions: [
                    {
                        amount: {
                            currency: currency ? currency : "EUR",
                            total: price,
                        },
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.execute(
            // @ts-ignore
            paymentId, execute_payment_json, function (error, payment) {
                if (error) {
                    res.render("cancel");
                }
                if (payment) {
                    custonorder_1.default.findOneAndUpdate({ _id: order }, {
                        estado: "Pagado",
                        progreso: "25",
                        status: "active",
                        pagoPaypal: payment,
                    }, (err, orde) => {
                        if (orde) {
                            if (scheduleTime_1.scheduleTime(ScheduledMaria_1.getMariaHorario().schedule)) {
                                asingRidersCustonOrder_1.AsingRiderCustonOrder(orde);
                            }
                            res.render("success");
                        }
                    });
                }
            });
        });
        this.router.get("/cancel-custom", (req, res) => {
            res.render("cancel");
        });
    }
}
const paypalRouter = new PaypalRouter();
paypalRouter.routes();
exports.default = paypalRouter.router;
