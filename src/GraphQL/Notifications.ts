import { IOrders } from "../models/newOrder/order";
import { sendNotification } from "./sendNotificationRider";
import { sendNotificationCustomer } from "./sendNotificationCustomer";
export const pushNotificationOrderProccess = (
  status: string,
  order: IOrders
) => {
  switch (status) {
    case "Confirmada":
      sendNotificationCustomer(
        order.customerData.OnesignalID,
        "Buenas noticias tu pedido ha sido confirmado 💥",
        "💥💥 Tu pedido ha sido confirmado"
      );
      break;

    case "En la cocina":
      sendNotificationCustomer(
        order.customerData.OnesignalID,
        "BREAKING NEWS: ¡tu pedido ya se está preparando en la cocinas!",
        "🥗 🥘 Tu pedido está en la cocina"
      );
      break;

    case "Listo para recoger":
      if (order.storeData.categoryName == "Restaurantes") {
        sendNotificationCustomer(
          order.customerData.OnesignalID,
          "Tu pedido está a punto de salir de la cocina",
          "🥡 🥢 Tu pedido está a punto de salir de cocina"
        );
      } else {
        sendNotificationCustomer(
          order.customerData.OnesignalID,
          "Tu pedido está listo para que el rider lo recoja",
          "Tu pedido está listo para recoger"
        );
      }

      if (order.courier) {
        sendNotification(
          order.courierData.OnesignalID,
          "El pedido ya está listo para recoger",
          "🚲 🛵 Pedido listo para recoger",
          false
        );
      }

      break;

    case "Preparando para el envío":
      sendNotificationCustomer(
        order.customerData.OnesignalID,
        "El repartidor está de camino al establecimiento para recoger el pedido",
        "🚲 El repartidor va de camino al establemiento"
      );
      break;

    case "En camino":
      sendNotificationCustomer(
        order.customerData.OnesignalID,
        "El repartidor está de camino para entregarte el pedido",
        "🛵 🍔 🍟 El repartidor va de camino"
      );
      break;

    case "Entregada":
      sendNotificationCustomer(
        order.customerData.OnesignalID,
        "⭐️ 🌟 El pedido ha sido entregado es hora de decirno que te ha parecido",
        "⭐️ 🌟 Pedido entregado, es hora de valorar"
      );
      break;

    case "Rechazado":
      sendNotificationCustomer(
        order.customerData.OnesignalID,
        "💔 El pedido ha sido rechazado lamentamos los incovenientes",
        "💔 Pedido rechazado"
      );
      break;

    case "Rechazada por la tienda":
      sendNotificationCustomer(
        order.customerData.OnesignalID,
        "💔 El pedido ha sido rechazado lamentamos los incovenientes",
        "💔 💔 Pedido rechazado por el establecomiento"
      );
      break;

    case "Devuelto":
      sendNotificationCustomer(
        order.customerData.OnesignalID,
        "💔 Hemos emitido una devolución del importe del pedido",
        "💔 Pedido devuelto"
      );
      break;

    case "Resolución":
      sendNotificationCustomer(
        order.customerData.OnesignalID,
        "Estmos evaluando tu caso pronto tendra noticias",
        "💌 💌Pedido en resolución"
      );
      break;

    default:
      break;
  }
};
