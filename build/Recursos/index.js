"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecursosUpdate = void 0;
const order_1 = __importDefault(require("../models/newOrder/order"));
const products_1 = __importDefault(require("../models/newMenu/products"));
const platos_1 = __importDefault(require("../models/nemu/platos"));
const menu_1 = __importDefault(require("../models/nemu/menu"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const collections_1 = __importDefault(require("../models/collections"));
const categorias_1 = __importDefault(require("../models/categorias"));
const RatingCalculartor_1 = require("../funtions/RatingCalculartor");
const rating_1 = __importDefault(require("../models/rating"));
const storeFormat_1 = require("../storeFormat");
const restaurant_2 = __importDefault(require("../models/restaurant"));
const products_2 = __importDefault(require("../models/newMenu/products"));
const category_1 = __importDefault(require("../models/newMenu/category"));
const bundles_1 = __importDefault(require("../models/newMenu/bundles"));
exports.RecursosUpdate = () => {
    //new restaurant(storeFormat()).save();
    const updateMainCategoria = () => __awaiter(void 0, void 0, void 0, function* () {
        const categorys = yield categorias_1.default.find();
        categorys.forEach((x) => {
            categorias_1.default.updateMany({ _id: x }, {
                excludeCity: [],
                visible: true,
                navigate: false,
                url: "",
            }, { upsert: true }, () => {
                console.log("done");
            });
        });
    });
    //updateMainCategoria();
    const setCategoryDuplicate = () => __awaiter(void 0, void 0, void 0, function* () {
        const cat = yield category_1.default.find({ account: "60faf41f0ea294528074a625" });
        cat.forEach((x, i) => __awaiter(void 0, void 0, void 0, function* () {
            const newCat = new category_1.default({
                name: x.name,
                description: x.description,
                account: "610003c7fb1a92e312078816",
                posLocationId: x.posCategoryId,
                posCategoryType: x.posCategoryType,
                subCategories: [],
                posCategoryId: "",
                imageUrl: "",
                products: x.products,
                menu: "610003c7fb1a92e312078816",
                storeId: "610003c7fb1a92e312078816",
                sortedChannelProductIds: [],
                subProductSortOrder: [],
                subProducts: [],
                level: 2,
                sorting: x.sorting,
                availabilities: [],
            });
            newCat.save().then(() => {
                console.log("save");
            });
        }));
    });
    const updateProductStoreID = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield products_2.default.find({ account: "615f53f30cc9d274ab4304d0" });
        g.forEach((x) => {
            products_2.default.updateMany({ _id: x }, {
                //@ts-ignore
                storeId: ["60faf41f0ea294528074a625", "610003c7fb1a92e312078816"],
            }, { upsert: true }, () => {
                console.log("done");
            });
        });
    });
    const updateOrders = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield order_1.default.find({ status: "Listo para recoger" });
        //@ts-ignore
        g.forEach((x) => __awaiter(void 0, void 0, void 0, function* () {
            const st = { status: "Entregada", date: new Date() };
            order_1.default.findOneAndUpdate({ _id: x === null || x === void 0 ? void 0 : x._id }, {
                status: "Entregada",
                $push: { statusProcess: st },
            }, { upsert: true }, () => {
                console.log("done");
            });
        }));
    });
    const updateProducVariables = () => __awaiter(void 0, void 0, void 0, function* () {
        const products = yield products_2.default.find({
            account: "61a7a1bbc5cc0874468cc368",
        });
        //@ts-ignore
        products.forEach((x) => __awaiter(void 0, void 0, void 0, function* () {
            products_2.default.findOneAndUpdate({ _id: x === null || x === void 0 ? void 0 : x._id }, {
                //@ts-ignore
                subProducts: ["61b899a18585936d282b35f4"],
            }, { upsert: true }, () => {
                console.log("done");
            });
        }));
    });
    //updateProducVariables();
    const updateProducBundled = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield bundles_1.default.findOne({ _id: "615f53f30cc9d274ab4304d0" });
        g &&
            g.subProducts.forEach((x) => __awaiter(void 0, void 0, void 0, function* () {
                const prod = yield products_2.default.findOne({ _id: x });
                products_2.default.findOneAndUpdate({ _id: prod === null || prod === void 0 ? void 0 : prod._id }, {
                    multiply: 2,
                    multiMax: 2,
                    max: 2,
                    min: 2,
                }, { upsert: true }, () => {
                    console.log("done");
                });
            }));
    });
    //updateProducBundled();
    const updateCollection = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield collections_1.default.find({
            store: "5fbd841981fafb0aa4c06479",
        });
        g.forEach((x) => {
            collections_1.default.updateMany({ _id: x._id }, {
                store: [
                    "5fbd841981fafb0aa4c06479",
                    "619a9322c7c9ee895da8dc21",
                    "619a918ac7c9ee895da8dc20",
                ],
            }, { upsert: true }, () => {
                console.log("done");
            });
        });
    });
    //updateCollection();
    const updateRestaurant = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield restaurant_2.default.find({ city: "Alcázar de San Juan" });
        g.forEach((x, i) => __awaiter(void 0, void 0, void 0, function* () {
            const ratings = yield rating_1.default.find({ restaurant: x._id });
            const averageRating = RatingCalculartor_1.RatingCalculator(ratings ? ratings : []);
            restaurant_2.default.findOneAndUpdate({ _id: x._id }, { rating: `${averageRating.toFixed(1)}` }, () => {
                console.log("done");
            });
        }));
    });
    const updateRestaurantOne = () => __awaiter(void 0, void 0, void 0, function* () {
        restaurant_2.default.updateMany({ _id: "60fc33acfbf3fe598d4c2a36" }, storeFormat_1.setHorario(), { upsert: true }, () => {
            console.log("done");
        });
    });
    //updateRestaurantOne();
    const updateProd = () => __awaiter(void 0, void 0, void 0, function* () {
        const g = yield products_2.default.find({ account: "610003c7fb1a92e312078816" });
        g.forEach((x) => {
            const price = (x.price * 10) / 100;
            products_2.default.updateMany({ _id: x._id }, {
                price: x.previous_price,
                previous_price: 0,
                offert: false,
            }, { upsert: true }, () => {
                console.log("done");
            });
        });
    });
    //updateProd();
    //updateRestaurant()
    //updateCollection()
    //update();
    /*  Order.deleteMany({ status: "Pendiente de pago" }).then(() => {
      console.log("hecho");
    }); */
    /* product.deleteMany({ account: "613b4b5e63541c8ba258a160" }).then(() => {
    console.log("hecho");
  }); */
    /* category.deleteMany({ account: "60faf41f0ea294528074a602" }).then(() => {
    console.log("hecho");
  }); */
    const setCategoryFronViejas = () => __awaiter(void 0, void 0, void 0, function* () {
        const cat = yield menu_1.default.find({
            restaurant: "5ff1d57185c1c068931a3860",
        });
        cat.forEach((x, i) => __awaiter(void 0, void 0, void 0, function* () {
            const pro = yield platos_1.default.find({ menu: x._id });
            const sub = pro.map((y) => {
                //@ts-ignore
                return y._id;
            });
            const newCat = new restaurant_1.default({
                name: x.title,
                description: "",
                account: "5ff1d57185c1c068931a3860",
                posLocationId: "",
                posCategoryType: "",
                subCategories: [],
                posCategoryId: "",
                imageUrl: "",
                products: sub,
                menu: "60be7ee0395b91df865649d5",
                storeId: "5ff1d57185c1c068931a3860",
                sortedChannelProductIds: [],
                subProductSortOrder: [],
                subProducts: [],
                level: 2,
                sorting: i,
                availabilities: [],
            });
            newCat.save().then(() => {
                console.log("save");
            });
        }));
    });
    /* setOrders().then(() => {
    console.log("guardado");
  }); */
    const setMenu = () => __awaiter(void 0, void 0, void 0, function* () {
        const data = yield platos_1.default.find({
            restaurant: "5fe4a2f5a55336531bb12a51",
        });
        data.forEach((x, i) => __awaiter(void 0, void 0, void 0, function* () {
            //@ts-ignore
            const Pric = Number(x.price) * 100;
            const pri = Pric.toFixed(0);
            const plato = {
                _id: x._id,
                //@ts-ignore
                name: x.title,
                //@ts-ignore
                description: x.ingredientes,
                nameTranslations: {},
                descriptionTranslations: {},
                //@ts-ignore
                account: x.restaurant,
                //@ts-ignore
                location: x.restaurant,
                productType: 1,
                plu: "",
                price: Number(pri),
                previous_price: null,
                priceLevels: {},
                sortOrder: i,
                deliveryTax: 0,
                takeawayTax: 0,
                multiply: 1,
                multiMax: 1,
                posProductId: "",
                posProductCategoryId: [],
                subProducts: [],
                productTags: [],
                posCategoryIds: [],
                //@ts-ignore
                imageUrl: x.imagen,
                max: 1,
                min: 1,
                capacityUsages: [],
                //@ts-ignore
                parentId: x.menu,
                visible: true,
                snoozed: false,
                subProductSortOrder: [],
                internalId: x._id,
                new: false,
                popular: false,
                offert: false,
                recomended: false,
                quantity: 1,
                storeId: "5fe4a2f5a55336531bb12a51",
            };
            new products_1.default(plato).save().then(() => {
                console.log("done");
            });
        }));
    });
    //setMenu()
    //console.log(new Date());
};
