import request from "request";
import { key, urlRiders } from "./config";
import orderSchema from "../models/newOrder/order";
import { datas } from "./dataRider";

export const setToHolded = (data: any, total: number, id: number) => {
  var optionsRiders = {
    method: "POST",
    url: urlRiders,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      key: key,
    },
    body: JSON.stringify(datas(data, total, id)),
  };

  request(optionsRiders, function (error, response) {
    if (error) throw new Error(error);
    const respo = JSON.parse(response.body);

    orderSchema.findOneAndUpdate(
      { _id: id },
      {
        $set: {
          holdedPartnerID: respo.id,
        },
      },
      (err, order) => {}
    );
  });
};
