import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const transSchema = new mongoose.Schema(
  {
    estado: {
      type: String,
    },
    total: {
      type: String,
    },
    fecha: {
      type: Date,
    },
    restaurantID: {
      type: String,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface ITran extends Document {
  estado: string;
  total: String;
  fecha: Date;
  restaurantID: number;
}

export default mongoose.model<ITran>("transacciones", transSchema);
