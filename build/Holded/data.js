"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.datasHolded = void 0;
exports.datasHolded = (data) => {
    const items = data.items.map((element) => {
        const t = element && element.items && element.items.subItems
            ? element.items.subItems
            : [];
        let pric = 0;
        let pric2 = 0;
        t.forEach((x) => {
            const r = x && x.subItems ? x.subItems : [];
            r.forEach((y) => {
                pric2 += y.price;
            });
            pric += x.price;
        });
        const precio = element.items.price + pric + pric2;
        const item = {
            name: element.items.name,
            desc: element.items.description,
            units: element.items.quantity,
            subtotal: precio / 100,
        };
        return item;
    });
    const dat = [
        {
            name: "Servicio de envío Wilbby",
            desc: "Costo de Envío",
            units: 1,
            subtotal: Number(data.deliveryCost) / 100,
        },
        {
            name: "Costes por transacción",
            desc: "Otros cargos",
            units: 1,
            subtotal: Number(data.serviceCharge) / 100,
        },
    ];
    return {
        applyContactDefaults: false,
        contactName: `${data.customerData.name} ${data.customerData.lastName}`,
        contactEmail: data.customerData.email,
        contactCity: data.customerData.city,
        desc: "Pedido Wilbby",
        date: data.deliveryTime,
        notes: "Gatos de envío y tarifas de servicio incluido en el total",
        items: items.concat(dat),
        invoiceNum: Number(data.channelOrderDisplayId),
        currency: "EUR",
        paymentMethodId: "605671642ca976325330606f",
    };
};
