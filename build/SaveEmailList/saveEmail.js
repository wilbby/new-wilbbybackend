"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaveEmailWilbby = void 0;
const EmailsDatas_1 = __importDefault(require("../models/EmailsDatas"));
exports.SaveEmailWilbby = (email, name, lastName) => {
    const newEmail = new EmailsDatas_1.default({
        name: name,
        lastName: lastName,
        email: email,
    });
    newEmail.save();
};
