"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const postSchema = new mongoose_1.default.Schema({
    title: {
        type: String,
        required: true,
        text: true,
        unique: true,
    },
    image: {
        type: String,
        required: true,
        text: true,
    },
    shortDescription: {
        type: String,
        required: true,
        unique: true,
    },
    like: {
        type: Number,
        default: 0,
    },
    tags: {
        type: [String],
    },
    author: {
        type: String,
    },
    category: {
        type: String,
    },
    readTime: {
        type: String,
    },
    content: {
        type: String,
    },
    slug: {
        type: String,
    },
    country: {
        type: String,
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("post", postSchema);
