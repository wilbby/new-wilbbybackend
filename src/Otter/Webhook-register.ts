import {
  WEB_HOOK_URL,
  Webhook_Secret,
  orderURL,
  user_Agent,
  client_id,
} from "./config";
import { IOrder } from "../models/custonorder";
var request = require("request");

export const registerWebHookURL = (order: IOrder, data: any) => {
  const input = {
    eventId: data.externalIdentifiers.id,
    eventTime: order?.created_at,
    eventType: "orders.order_status_update",
    metadata: {
      storeId: data.storeId,
      applicationId: client_id,
      resourceHref: `${orderURL}/${data.externalIdentifiers.id}`,
      resourceId: data.externalIdentifiers.id,
      payload: {
        orderStatusHistory: [
          {
            status: "RECIVED_ORDER",
            eventTime: new Date(),
          },
        ],
      },
    },
  };

  const options = {
    method: "POST",
    url: WEB_HOOK_URL,
    headers: {
      "Content-Type": "application/json",
      Authorization: `MAC ${Webhook_Secret}`,
      "User-Agent": user_Agent,
    },
    body: JSON.stringify(input),
  };

  request(options, function (error, response) {
    console.log(response.body);
  });
};
