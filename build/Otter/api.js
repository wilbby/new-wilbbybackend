"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pushOrderToOtter = void 0;
const config_1 = require("./config");
const data_1 = require("./data");
const Webhook_register_1 = require("./Webhook-register");
var request = require("request");
var details = {
    client_id: config_1.client_id,
    client_secret: config_1.client_secret,
    grant_type: "client_credentials",
    scope: "orders.create",
};
let formBody = [];
for (var property in details) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(details[property]);
    //@ts-ignore
    formBody.push(encodedKey + "=" + encodedValue);
}
//@ts-ignore
formBody = formBody.join("&");
var options = {
    method: "POST",
    url: config_1.authURL,
    headers: {
        "Content-Type": "application/x-www-form-urlencoded",
    },
    body: formBody,
};
exports.pushOrderToOtter = (order) => {
    request(options, function (error, response) {
        const auth = JSON.parse(response.body);
        var option = {
            method: "POST",
            url: config_1.orderURL,
            headers: {
                "Content-Type": "application/json",
                "X-Application-Id": config_1.client_id,
                "X-Store-Id": order.storeData.OtterPartnerId,
                Authorization: `Bearer ${auth.access_token}`,
            },
            body: JSON.stringify(data_1.data(order)),
        };
        request(option, function (error, response) {
            const res = JSON.parse(response.body);
            //@ts-ignore
            Webhook_register_1.registerWebHookURL(order, res);
        });
    });
};
